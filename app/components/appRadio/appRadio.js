angular.module('myApp.directive.radio', [])

// 自定义指令
.directive('appRadio', function() {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'components/appRadio/appRadio.html',
        scope: {
            label: "@",
            name: '=',
            ngModel: '=',
            list: '='
        },
        link: function($scope) {}
    }
})