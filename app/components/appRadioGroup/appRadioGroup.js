angular.module('myApp.directive.radioGroup', [])

// 自定义指令
.directive('appRadioGroup', function() {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'components/appRadioGroup/appRadioGroup.html',
        scope: {
            label: "@",
            name: '=',
            ngModel: '=',
            list: '='
        },
        link: function($scope) {}
    }
})