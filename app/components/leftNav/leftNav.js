/**
 * 头部信息，展示logo和右侧操作按键
 */

angular.module('myApp.directive.leftNav', [])
// 定义一个指令
.directive('leftNav', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'components/leftNav/leftNav.html',
        // 需要执行的控制器
        controller: function($scope, $CONST, $user, $menu) {
            // 菜单项
            $scope.menuGroup = $menu.getUserMenu();
            // 如果登录后，刷新一下用户菜单
            $user.setCallback(function() {
                // 菜单项
                $scope.menuGroup = $menu.getUserMenu();
            });
            /**
             * 根据传入的参数来展开菜单
             *
             * @param { Number  } length 菜单长度
             * @param { Boolean } isOpen 菜单是否打开了
             */
            $scope.openOrCloseMenu = function(length, isOpen) {
                // 如果打开了菜单（加上父菜单的高度）
                if (isOpen) {
                    return { 'height': $CONST.MENU_HEIGHT * (length + 1) + 'px' };
                }
                return { 'height': $CONST.MENU_HEIGHT + 'px' };
            };

            /**
             * 是否选中导航栏
             *
             * @param { Object } menu 当前点击的菜单
             */
            $scope.openMenuItem = function(menu) {
                // 先记录当前的打开状态
                var open = menu.open;
                // 先将所有的菜单关闭
                angular.forEach($scope.menuGroup, function(value) {
                    value.open = false;
                });
                // 如果当前是开启，则关闭；如果是关闭，则开启
                menu.open = !open;
            };

            /**
             * 打开二级菜单
             */
            $scope.selectSecondItem = function(firstMenuName, secondMenuName) {
                // 改变面包屑导航
                $scope.$emit('menu-change', [firstMenuName, secondMenuName]);
            };

            // 改变面包屑导航
            $scope.$emit('menu-change', ['首页']);
        }
    }
});
