angular.module('myApp.directive.page', [])
// 分页组件
    .directive('appPage', function () {
        return {
            restrict: 'E',
            scope: {
                current: '=',
                totals: '=',
                fn: '='
            },
            templateUrl: 'components/page/page.html',
            link: function (scope) {
                angular.element('.tcdPageCode').createPage({
                    pageCount: scope.totals,
                    current: scope.current,
                    backFn: function (p) {
                        scope.fn && scope.fn(p)
                    }
                });
            }
        }
    });
