angular.module('myApp.directive.popUp', [])

// 自定义指令
    .directive('popUp', function() {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                name: '@',
                show: '=',
                list: '=',
                on: '&',
                height: '@',
                button: '=',
                tip: '@' // 错误提示信息
            },
            templateUrl: 'components/popUp/popUp.html',
            link: function($scope) {
                /**
                 * 关闭弹框
                 */
                $scope.closePopUp = function() {
                    $scope.show = false;
                };
            }
        }
    });