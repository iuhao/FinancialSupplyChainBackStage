angular.module('myApp.directive.checkbox', [])

// 自定义指令
.directive('appCheckbox', function() {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'components/appCheckbox/appCheckbox.html',
        scope: {
            label: "@",
            name: '=',
            ngModel: '=',
            list: '='
        },
        link: function($scope) {
            /**
             * 判断当前是否被选中了
             */
            $scope.isChecked = function(id) {
                // 如果没有传入值
                if (!$scope.ngModel) {
                    return false;
                }
                return $scope.ngModel.inArray(id);
            };

            /**
             * 点击选中，或者不选中复选框
             *
             * @param item 当前复选框对象
             */
            $scope.chooseCheckbox = function(item) {
                // 如果双向绑定的值没有传入，则将其初始化成空数组
                if (!$scope.ngModel) {
                    $scope.ngModel = [];
                }
                // 如果已经选中过
                if ($scope.ngModel.indexOf(item.value) != -1) {
                    $scope.ngModel.splice($scope.ngModel.indexOf(item.value), 1);
                    return;
                }
                // 如果没有选中过，则将当前点击的放到绑定的对象中
                $scope.ngModel.push(item.value);
            }
        }
    }
});