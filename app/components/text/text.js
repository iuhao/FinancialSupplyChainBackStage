angular.module('myApp.directive.text', [])

// 自定义指令
.directive('appText', function() {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'components/text/text.html',
        scope: {
            list: "="
        },
        link: function(scope) {}
    }
});