angular.module('myApp.directive.checkboxGroup', [])

// 自定义指令
.directive('appCheckboxGroup', function() {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'components/appCheckboxGroup/appCheckboxGroup.html',
        scope: {
            label: "@",
            name: '=',
            ngModel: '=',
            list: '='
        },
        link: function($scope) {
            // 遍历数据
            angular.forEach($scope.list, function(value) {
                angular.forEach(value.list, function(list) {
                    if ($scope.ngModel && $scope.ngModel.inArray('' + list.value)) {
                        list.checked = true;
                    }
                });
            });

            /**
             * 点击选中，或者不选中复选框
             *
             * @param item 当前复选框对象
             */
            $scope.chooseCheckbox = function(item) {
                // 如果双向绑定的值没有传入，则将其初始化成空数组
                if (!$scope.ngModel) {
                    $scope.ngModel = [];
                }
                // 如果已经选中过
                if ($scope.ngModel.indexOf('' + item.value) != -1) {
                    $scope.ngModel.splice($scope.ngModel.indexOf('' + item.value), 1);
                    return;
                }
                // 如果已经选中过
                if ($scope.ngModel.indexOf(item.value) != -1) {
                    $scope.ngModel.splice($scope.ngModel.indexOf(item.value), 1);
                    return;
                }
                // 如果没有选中过，则将当前点击的放到绑定的对象中
                $scope.ngModel.push(item.value);
            }
        }
    }
});