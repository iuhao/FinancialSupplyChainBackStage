angular.module('myApp.directive.appTextGroup', [])

// 自定义指令
.directive('appTextGroup', function() {
    return {
        restrict: 'EAC',
        replace: true,
        templateUrl: 'components/appTextGroup/appTextGroup.html',
        scope: {
            // 列表
            list: '='
        },
        link: function(scope) {}
    }
});