/**
 * 头部信息，展示logo和右侧操作按键
 *
 * Created by haohao on 2017/10/18.
 */

angular.module('myApp.directive.list', ['ngRoute'])
  // 定义一个指令
  .directive('list', function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'components/list/list.html',
      scope: {
        titleList: '=',
        contentList: '=',
        versionName: '='
      },
      // 需要执行的控制器
      controller: function($scope,$location) {
        // 修改
        $scope.modify = function(ele, ver) {
          if (ver == 'creaditM' || ver == 'creaditMCom') {
            $location.path('creditdetialcom');
            return;
          }
          angular.element('#myModal').modal('show');
          $scope.$emit('title',ele);
        };
        // 删除
        $scope.delete = function(ver) {
          alert(22);
        };
        // 关联柜员
        $scope.linkServer = function(ele) {
          angular.element('#myModal').modal('show');
          $scope.$emit('title',ele);
        };
        // 通知客户
        $scope.popCustomer = function(ele) {
          angular.element('#myModal').modal('show');
          $scope.$emit('title',ele);
        }
      }
    }
  });
