/**
 * 自定义指令 —— 面包屑导航，和菜单联动，当点击菜单的时候，动态改变面包屑导航
 */
angular.module('myApp.directive.breadCrumb', [])
  .directive("breadCrumb", function() {
    return {
      restrict: "E",
      replace: true,
      scope: {
        menus: '='
      },
      templateUrl: 'components/breadCrumb/breadCrumb.html',
      controller: function($scope, $location) {
      }
    }
  });
