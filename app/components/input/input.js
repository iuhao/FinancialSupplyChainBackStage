angular.module('myApp.directive.input', [])

// 自定义指令
.directive('appInput', function($valid, $CONST) {
    return {
        restrict: 'EAC',
        replace: true,
        templateUrl: 'components/input/input.html',
        scope: {
            // 提示信息
            tip: '@',
            // 标题
            label: '@',
            // 是否必填
            must: '=',
            // input的name属性
            name: '=',
            // 当type是input-unit时展示单位
            unit: '@',
            // 类型
            type: '@',
            // 是否禁用
            disabled: '=',
            // 当type是select时，下拉的值
            options: '=',
            // 双向绑定的值
            ngModel: '=',
            // 是否只读
            readonly: '=',
            // 当type是text时，需要展示的值
            text: '=',
            // 校验
            pattern: '@',
            // 将校验结果通知上级组件
            validate: '=',
            // 输入框提示
            placeholder: '@'
        },
        link: function(scope) {
            // 第一次进入
            scope.isFirstIn = true;

            // 先分割名字，获得集合
            scope.names = scope.name?scope.name.split(','): [];

            // 如果当前是下拉选择框
            if (scope.type == 'select') {
                angular.forEach(scope.options, function(value) {
                    if (value.selected) {
                        scope.ngModel = value;
                    }
                });
                // 添加监听
                var unwatch = scope.$watch('ngModel', function(newValue, oldValue) {
                    // 如果新值和旧值一样，则返回
                    if (JSON.stringify(newValue) == JSON.stringify(oldValue)) {
                        return;
                    }
                    // 如果双向绑定的值不存在，则取消监听
                    if (!newValue) {
                        scope.ngModel = oldValue;
                        unwatch();
                        return;
                    }
                    angular.forEach(scope.options, function(value) {
                        if (newValue == value.value || newValue.value == value.value) {
                            scope.ngModel = value;
                        }
                    });
                    unwatch();
                });
            }

            /**
             * 校验规则
             */
            scope.validateRule = function() {
                // 如果是只读，则不校验
                if (scope.readonly) {
                    return;
                }

                // 不是第一次进来（点击过了）
                scope.isFirstIn = false;

                // 清空错误信息
                scope.isError = '';

                // 初始化下此时的状态
                scope.setValidity();

                // 如果是必填，但是没有输入值（输入框）
                if (scope.must && !scope.ngModel && scope.type == 'input') {
                    scope.isError = '此项是必填项';
                    // 保存校验成功或失败状态
                    scope.setValidity();
                    return
                }

                // 如果是必填，但是没有输入值（时间选择）
                if (scope.must && !scope.ngModel && scope.type == 'time') {
                    scope.isError = '此项是必填项';
                    // 保存校验成功或失败状态
                    scope.setValidity();
                    return
                }

                // 如果是必填，但是没有选择（下拉选择）
                if (scope.must && (!scope.ngModel || !scope.ngModel.value) && scope.ngModel.value != 0 && scope.type == 'select') {
                    scope.isError = '此项是必填项';
                    // 保存校验成功或失败状态
                    scope.setValidity();
                    return
                }

                // 如果校验不存在，则直接抛出
                if (!scope.pattern) {
                    return
                }

                // 分割需要校验的规则
                var rule = scope.pattern.split('|');

                // 遍历规则数据
                angular.forEach(rule, function(value) {
                    if (!scope.isError && scope.ngModel) {
                        scope.isError = $valid[value](scope.ngModel);
                    }
                });
                // 保存校验成功或失败状态
                scope.setValidity();
            };

            /**
             * 设置失败的校验
             */
            scope.setValidity = function() {
                // 如果没有校验，则抛出
                if (scope.validate == null || scope.validate == undefined) {
                    return;
                }
                // 如果校验规则是字符串
                if (typeof scope.validate == 'boolean') {
                    // 如果只有一个name，则直接生成校验结果
                    scope.validate = !Boolean(scope.isError);
                    // 让父级校验全部
                    scope.$emit('$validate');
                }
            };
        }
    }
})