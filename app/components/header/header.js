/**
 * 头部信息，展示logo和右侧操作按键
 *
 * Created by haohao on 2017/10/16.
 */

angular.module('myApp.directive.header', [])
    // 定义一个指令
    .directive('header', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'components/header/header.html',
            // 需要执行的控制器
            controller: function ($scope, $location, $model, $timeout, $user, $http, $toast, $CONST, api) {
                // 获取用户登录名
                $scope.name = $user.get('user_login_name');
                // 获取角色常量
                $scope.role = $CONST.WARNING_ROLE_LIST;
                // 用户角色
                $scope.userRole = $user.get('user_role');

                // 如果没有获取到用户名
                if (!$scope.name) {
                    $location.path('/login');
                }

                /**
                 * 展开弹框
                 */
                $scope.showPopUp = function() {
                    // 展开弹框
                    $scope.show = true;

                    // 弹出框
                    $scope.list = [
                        { label: '重设密码', type: 'password', value: '', must: true, placeholder: '请输入新的登录密码' },
                        { label: '确认密码', type: 'password', value: '', must: true, placeholder: '请再次输入新的登录密码' }
                    ];
                };

                /**
                 * 设置回调，如果用户信息更新，则直接刷新用户信息
                 */
                $user.setCallback(function() {
                    // 获取用户信息
                    $scope.userInfo = $user.getUser();
                    // 获取用户登录名
                    $scope.name = $user.get('user_login_name');
                    // 用户角色
                    $scope.userRole = $user.get('user_role');
                });

                /**
                 * 修改密码
                 */
                $scope.modifyPw = function () {
                    // 密码没有输入
                    if (!$scope.list[0].value) {
                        $toast('请输入重设密码');
                        return;
                    }
                    // 2次密码输入的不一致
                    if ($scope.list[0].value != $scope.list[1].value) {
                        $toast('您二次密码输入的不一致，请确认');
                        return;
                    }
                    // 调用接口
                    api.modifyPwd({
                        id: $user.get('id'),
                        new_pass: $scope.list[0].value
                    }).then(function () {
                        $scope.show = false;
                        $model._toast('修改密码成功', function () {
                            $location.path('/login')
                        });
                    });
                };

                /**
                 * 退出
                 */
                $scope.quickOut = function () {
                    $user.removeUser();
                    $model._toast('退出成功', function () {
                        $location.path('/login')
                    })
                };
            }
        }
    });
