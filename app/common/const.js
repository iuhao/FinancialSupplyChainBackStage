angular.module('myApp.const.all', [])
    .constant('$CONST', {
        // 正式环境
        //$IP: 'http://116.62.197.248/FinanceChain/api/',
        // 测试环境代码
        //$IP: 'http://218.94.44.226:8820/FinanceChain/api/',
        // 测试内网环境
        //  $IP: 'http://192.168.2.17:8820/FinanceChain/api/',
        // 匡亮的IP
        $IP: 'http://192.168.4.22:8080/FinanceChain/api/',
          //  袁卫华
          //  $IP:'http://192.168.2.171:8080/FinanceChain/api/',
        // 系统初始设置
        CONFIG: {
            PWD: 'admin123'
        },
        // 不能删除和修改的角色id 依次为：超级管理员，管理员，柜员，风控员，分配员
        WARNING_ROLE: [1, 17, 32, 33, 34],
        // 角色列表
        WARNING_ROLE_LIST: {
            1: '超级管理员',
            17: '管理员',
            32: '柜员',
            33: '风控员',
            34: '分配员'
        },
        // 菜单长度
        MENU_HEIGHT: 56,
        // 成功状态码
        SUCCESS: '200',
        // 已婚状态
        MARRIAGE_STATE: [
            {
                label: '未婚',
                value: 0,
                selected: true
            },
            {
                label: '已婚',
                value: 1
            }
        ],
        // 家庭年收入
        FAMILY_INCOME: [
            {
                label: '5万元以下',
                value: 0,
                selected: true
            },
            {
                label: '5-10万元',
                value: 1
            },
            {
                label: '10-15万元',
                value: 2
            },
            {
                label: '15-25万元',
                value: 3
            },
            {
                label: '25万以上',
                value: 4
            }
        ],
        // 订单来源
        ORDER_ORIGIN: [
            {
                label: '请选择',
                value: '',
                selected: true
            },
            {
                label: '好下单',
                value: 1
            }
        ],
        // 证件类型
        ID_TYPE: [
            {
                value: '01',
                label: '组织结构代码证'
            },
            {
                value: '02',
                label: '营业执照'
            },
            {
                value: '03',
                label: '社会信用号'
            },
            {
                value: '04',
                label: '身份证号码',
                selected: true
            }
        ],
        // 贷款类型
        LOAN_TYPE: [
            {
                value: 0,
                label: '请选择'
            },
            {
                value: 1,
                label: '订单贷款'
            },
            {
                value: 2,
                label: '其他贷款'
            }
        ],
        // 审核状态
        AUDIT_STATUS: [{value: '1', label: '待审核'}, {value: '2', label: '审核通过'}, {value: '3', label: '审核不通过'}],
        // 不动产抵押
        REAL_ESTATE: [{value: 1, label: '有', selected: true}, {value: 2, label: '无'}],
        // 错误码
        ERROR_CODE: {
            NO_LOGIN: '您的登录信息失效，请重新登录！',
            201: '亲，您操作的该条信息不完整，缺少了必要字段哦！',
            301: '亲，您已经注册过，直接去登录吧！',
            302: '您的登录信息失效，请重新登录！',
            310: '亲，您已经注册过，直接去登录吧！',
            311: '亲，您的手机号还没注册哦！',
            320: '贷款结果已通知客户！',
            360: '亲，你创建的机构已经存在咯，检查一下名字吧！',
            10001: '亲，您的手机号还没注册哦！',
            400: '亲，没有查询到此贷款用户的相关贷款信息哦！',
            10002: '亲，您的用户名或者密码输入有误哦！',
            10004: '亲，您的验证码已经失效了！',
            10005: '亲，您的验证码输入的不对哦！',
            100000: '网络异常，请重试！'
        },
        // 菜单
        MENUS: [
            {
                id: "collapseOneS",
                name: "系统管理",
                open: false,
                children: [
                    {
                        id: "collapseOneS",
                        name: "管理员管理",
                        href: "system/account"
                    }, {
                        id: "collapseOneS",
                        name: "角色列表",
                        href: "system/role"
                    }
                ]
            },
            {
                id: "collapseOneD",
                name: "客户分配",
                open: false,
                children: [
                    {
                        id: "collapseOneS",
                        name: "个人分配",
                        href: "distribution/person"
                    }, {
                        id: "collapseOneS",
                        name: "企业分配",
                        href: "distribution/company"
                    }
                ]
            },
            {
                id: "collapseOneB",
                name: "授信管理",
                open: false,
                children: [
                    {
                        id: "collapseOneS",
                        name: "个人授信",
                        href: "award/person"
                    }, {
                        id: "collapseOneS",
                        name: "企业授信",
                        href: "award/company"
                    }
                ]
            },
            {
                id: "collapseOneB",
                name: "机构管理",
                open: false,
                children: [
                    {
                        id: "collapseOneS",
                        name: "个人",
                        href: "institution/person"
                    }, {
                        id: "collapseOneS",
                        name: "企业",
                        href: "institution/person"
                    }
                ]
            }
        ],

        // 柜员--贷款状态
        LOAN_STATE: [
            { label: '全部', value: '', selected: true },
            { label: '待审核(柜员)', value: 1 },
            { label: '审核通过(柜员)', value: 2 },
            { label: '审核不通过(柜员)', value: 3 },
            { label: '审核通过(风控)', value: 4 },
            { label: '审核不通过(风控)', value: 5 }
        ],

        // 分配列表
        ALLOT_STATE: [
            {label: '全部', value: '', selected: true},
            {label: '待分配', value: 1},
            {label: '已分配', value: 2}
        ]
    });