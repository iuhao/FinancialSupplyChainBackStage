/**
 * 获取文件真实路径
 * @param { Object } fileObj 文件对象
 * @returns {*}
 */
var getFileRealPath = function(fileObj) {
    // 商品链接
    var src = '';
    var windowURL = window.URL || window.webkitURL;

    // 如果是高级别浏览器
    if (fileObj && fileObj.files && fileObj.files[0]) {
        return windowURL.createObjectURL(fileObj.files[0]);
    }
    return fileObj.value;
};

/**
 * sessionStorage的操作
 */
var session = {
    get: function(key) {
        return JSON.parse(sessionStorage.getItem(key));
    },
    set: function(key, value) {
        sessionStorage.setItem(key, JSON.stringify(value));
    },
    remove: function(key) {
        sessionStorage.removeItem(key);
    },
    clear: function() {
        sessionStorage.clear();
    }
};

/**
 * localStorage的操作
 */
var storage = {
    get: function(key) {
        return JSON.parse(localStorage.getItem(key));
    },
    set: function(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    },
    remove: function(key) {
        localStorage.removeItem(key);
    },
    clear: function() {
        localStorage.clear();
    }
};

/**
 * 将参数追加到url上
 *
 * @param { String } url    待追加的参数
 * @param { Object } params 参数
 */
var appendToUrl = function(url, params) {
    // 首先追加上一个？
    url += '?';
    // 将所有的参数追加上
    angular.forEach(params, function(value, index) {
        if (value) {
            url += index + '=' + value + '&'
        }
    });
    // 返回url链接，并删除最后一个&符号
    return url.substr(0, url.length - 1);
};

/**
 * 判断是否是boolean类型
 *
 * @param { Array } arr 待判断的数组
 */
var toBoolean = function(arr) {
    var flag = true;
    angular.forEach(arr, function(value)  {
        flag = flag && value;
    });
    return flag
};

/**
 * 判断一个值是否在数组中存在
 *
 * @param { Object / String / Number } data 需要校验的值
 */
Array.prototype.inArray = function (data) {
    // 获取当前数组
    var array = this;

    // 是否在数组中
    var isIn = false;

    // 遍历数组，比较数据，如果存在，则将标志位设置成true
    angular.forEach(array, function(value) {
        isIn = isIn || (JSON.stringify(value) == JSON.stringify(data));
    });
    return isIn;
};

// 格式化日期
Date.prototype.format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
};