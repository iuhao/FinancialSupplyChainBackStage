angular.module("myApp.config", [])
/**
 * 配置服务地址
 */
.config(function ($httpProvider) {
    $httpProvider.interceptors.push(function ($q, $model, $CONST, $user, $location) {
        return {
            // 加载请求
            'request': function(config) {
                // 判断接口的登录信息是否失效（暂只支持POST请求）
                if (config.method == 'POST' && !config.data.noValidate && !$user.get('accessToken')) {
                    // 弹框提示后跳转到登录页面
                    $model._toast($CONST.ERROR_CODE['NO_LOGIN'], function() {
                        $location.path('/login');
                    });
                    // 如果当前登录信息失效，则拒绝请求接口
                    return $q.reject($CONST.ERROR_CODE['NO_LOGIN']);
                }
                // 正常请求接口
                return config || $q.when(config);
            },
            // 处理返回报文
            'response': function (config) {
                // 如果是404,500等，则显示网络错误
                if (config.status != 200) {
                    $model._toast($CONST.ERROR_CODE[100000]);
                    return $q.reject($CONST.ERROR_CODE[100000]);
                }
                // 返回报文错误
                if (typeof config.data == 'object' && config.data.code != 200) {
                    if (config.data.code == 375 || config.data.code == 376) {
                        $model._toast($CONST.ERROR_CODE[config.data.code] || config.data.msg);
                        return config;
                    }
                    $model._toast($CONST.ERROR_CODE[config.data.code] || config.data.msg);
                    return $q.reject(config.data.msg);
                }
                return config;
            }
        };
    });
});
