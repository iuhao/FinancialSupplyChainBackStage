angular.module("myApp.directive.other", [])
/** 时间选择器，且可以动态绑定angular的ngModel对象上 */
.directive('wdatePicker', function() {
    return {
        restrict: 'A',
        scope: {
            minDate: "@",
            maxDate: "@"
        },
        require: "ngModel",
        link: function(scope, element, attrs, $ngModel) {

            // 当点击输入框的时候，加载日期组件
            element.bind("click, focus", function() {
                // 生成日期组件
                WdatePicker({

                    // 最大值
                    maxDate: (scope.maxDate || ''),

                    // 最小值
                    minDate: (scope.minDate || ''),

                    // 每次选择时间组件的时候触发的时间
                    onpicking: function(dp) {
                        // 更改noModel的值
                        $ngModel.$setViewValue(dp.cal.getNewDateStr('yyyy-MM-dd'));
                        $ngModel.$render();

                        // 刷新作用域
                        scope.$apply();
                    },
                    onclearing: function(dp) {

                        // 更改noModel的值
                        $ngModel.$setViewValue("");
                        $ngModel.$render();

                        // 刷新作用域
                        scope.$apply();
                    }
                });
            })

        }
    }
});

