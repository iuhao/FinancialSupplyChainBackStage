angular.module('myApp.filter.all', [])

.filter("loanState", function() {
    return function(input) {

        if (input == 1) {
            return "待审核";
        }

        if (input == 2) {
            return "审核通过";
        }

        if (input == 3) {
            return "审核失败";
        }

        if (input == 4) {
            return "已放款";
        }

        if (input == 5) {
            return "已还款（银行端）";
        }

        if (input == 6) {
            return "已还款（平台）";
        }
        return "--";
    };
})
/**
 * 定义审核状态的过滤器
 */
.filter("auditStatus", function() {
    return function(input) {

        if (input == 1) {
            return "审核中";
        }

        if (input == 2) {
            return "审核通过";
        }

        if (input == 3) {
            return "审核失败";
        }

        return "--";
    };
})
/**
 * 定义审核状态的过滤器
 */
.filter("creditsStatus", function() {
    return function(input) {

        if (input == 1) {
            return "待授信";
        }

        if (input == 2) {
            return "已授信";
        }

        if (input == 3) {
            return "授信失败";
        }

        return "--";
    };
})
/**
 * 定义抵押的过滤器
 */
.filter("loan_mortgage", function() {
    return function(input) {
        if (input == 1) {
            return "有";
        }

        if (input == 2) {
            return "无";
        }
        return "--";
    };
})
/**
 * 定义贷款方式的过滤器
 */
.filter("lnci_type", function() {
    return function(input) {
        if (input == 1) {
            return "订单贷款";
        }

        if (input == 2) {
            return "其他贷款";
        }
        return'--';
    };
})
/**
 * 定义家庭收入方式
 */
.filter("user_income_type", function() {
    return function(input) {
        if (input == 0) {
            return "5万以下";
        }
        if (input == 1) {
            return "5-10万元";
        }
        if (input == 2) {
            return "10-15万元";
        }
        if (input == 3) {
            return "15-25万元";
        }
        if (input == 4) {
            return "25万以上";
        }
        return'请选择家庭收入';
    };
})
/**
 * 定义家庭收入方式
 */
.filter("user_income_type_show", function() {
    return function(input) {
        if (input == 0) {
            return "5万以下";
        }
        if (input == 1) {
            return "5-10万元";
        }
        if (input == 2) {
            return "10-15万元";
        }
        if (input == 3) {
            return "15-25万元";
        }
        if (input == 4) {
            return "25万以上";
        }
        return '未填写';
    };
})
.filter("user_marital_desc",function(){
    return function(input){
        if (input == 0) {
            return "未婚";
        }
        if (input == 1) {
            return "已婚";
        }
        return'请选择婚姻状况';
    }
})
/**
 * 订单状态
 */
.filter("orderStatus", function() {
    return function(input) {
        if (input == 0) {
            return "订单取消";
        }
        if (input == 10) {
            return "已提交待付款";
        }
        if (input == 11) {
            return "已付定金";
        }
        if(input ==15){
              return "为线下付款提交申请(已经取消该付款方式)";
        }
        if (input == 16) {
            return "货到付款";
        }
          if (input == 18) {
                return "已付款30%";
          }
        if (input == 20) {
            return "已付款";
        }
        if (input == 21) {
            return "申请退款";
        }
        if (input == 22) {
            return "退款中";
        }
        if (input == 25) {
            return "退款完毕";
        }
        if (input == 30) {
            return "已发货";
        }
        if (input == 35) {
            return "自提点已经收货";
        }
        if (input == 40) {
            return "已收货";
        }
        if (input == 41) {
            return "申请退货";
        }
        if (input == 42) {
            return "退货审核中";
        }
        if (input == 45) {
            return "退货完成";
        }
        if(input == 46){
              return "退款及退货退款";
        }
        if (input == 50) {
            return "买家评价完毕";
        }
        if (input == 60) {
            return "已结束";
        }
        if (input == 65) {
            return "订单不可评价";
        }
        return '--';
    };
})
/**
 * 支付方式
 */
.filter("payType", function() {
    return function(input) {
        if (input == "online") {
            return "在线支付";
        }
        if (input == "payafter") {
            return "货到付款";
        }
        if (input == "contractPay") {
            return "合同付款";
        }
        return "--";
    };
})

/**
 * 证件类型
  */
.filter('idType', function() {
    return function(input) {
        if (input == '01') {
            return '组织结构代码证'
        }
        if (input == '02') {
            return '营业执照'
        }
        if (input == '03') {
            return '社会信用号'
        }
        if (input == '04') {
            return '身份证号码'
        }
        return '--'
    }
})
/**
 * 订单平台
 */
.filter('ptnum', function() {
    return function(input) {
        if (input == 'PT201709051287') {
            return '好下单'
        }
        return '未选择平台'
    }
})
/**
 *
 */
.filter("ifNull", function() {
    return function(input) {
        if (input == "" || !input ) {
            return "--";
        }

        return input;
    };
})
.filter("scoreStatus", function() {
    return function(input) {
        // 未评分
        if (input == 1) {
            return "未评分";
        }
        // 已评分
        if (input == 2) {
            return "已评分";
        }
        return "";
    };
})
.filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
});