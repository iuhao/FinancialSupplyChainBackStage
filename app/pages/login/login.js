angular.module('myApp.pages.login', [])
// 配置路由
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'pages/login/login.html',
        controller: 'LoginCtrl'
    })
}])
// 登录控制器
.controller('LoginCtrl', function($scope, $rootScope) {
    // 隐藏一切
    $rootScope.isHide = true;
});

