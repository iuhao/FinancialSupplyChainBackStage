angular.module('myApp.pages.institution', [])
// 定义路由
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/institution', {
            templateUrl: 'pages/institution/institution.html',
            controller: 'InstitutionCtrl'
        })
    }])
    // 界面控制器
    .controller('InstitutionCtrl', function ($scope, $location, $toast, $timeout, $user, $http, $CONST, api) {
        // 当前页
        $scope.currentPage = 1;
        // 总页数
        $scope.totalPage = 1;
        // 每页展示数量
        $scope.pageSize = 10;
        // 不展示弹出框
        $scope.show = false;

        /**
         * 打开新增机构的弹出框
         */
        $scope.addPopup = function () {
            // 弹出框展示的数据
            $scope.list = [
                {label: '机构名称', type: 'input', must: true, value: ''},
                {label: '联系人', type: 'input', must: true, value: ''},
                {label: '联系方式', type: 'input', must: true, value: ''},
                {label: '联系地址', type: 'textarea', must: true, value: ''},
                {label: '执行利率(月)', type: 'input', must: true, value: ''}
            ];

            // 标记当前是编辑状态
            $scope.type = 'add';

            // 打开弹出框
            $scope.show = true;
        };

        /**
         * 打开修改机构的弹出框
         *
         * @param { Object } item 当前机构对象
         */
        $scope.updatePopup = function (item) {
            // 保存当前机构对象，后面编辑的时候要用
            $scope.insitutionItem = item;

            // 弹出框编辑项
            $scope.list = [
                {label: '机构名称', type: 'input', must: true, value: item.institution_name},
                {label: '联系人', type: 'input', must: true, value: item.institution_linkman},
                {label: '联系方式', type: 'input', must: true, value: item.institution_linkphone},
                {label: '联系地址', type: 'textarea', must: true, value: item.institution_addr},
                {label: '执行利率(月)', type: 'input', must: true, value: item.institution_base_rate}
            ];

            // 标记当前是编辑状态
            $scope.type = 'edit';

            // 打开弹出框
            $scope.show = true;
        };

        /*
         * 错误信息
         * */
        $scope.errorMsgList = {
            NO_NULL: '*为必填项',
            NO_VALIDATE_NAME: '格式错误，机构名称或姓名为中文或英文',
            NO_VALIDATE_PHONE: '格式错误，手机号为11位数字',
            NO_VALIDATE_RATE: '格式错误，执行利率只能包含数字和.'
        };

        /**
         * 弹出框中点击确定，触发的事件（新增和编辑）
         */
        $scope.addOrUpdateInstitution = function () {
            // 校验*是否为空
            if (!$scope.list[0].value || !$scope.list[1].value || !$scope.list[2].value || !$scope.list[3].value || !$scope.list[4].value) {
                $scope.errorTip = $scope.errorMsgList.NO_NULL;
                return;
            }
            // 校验姓名是否符合正则
            if (!/^[\u4e00-\u9fa5a-zA-Z]+$/.test($scope.list[0].value) || !/^[\u4e00-\u9fa5a-zA-Z]+$/.test($scope.list[1].value)) {
                $scope.errorTip = $scope.errorMsgList.NO_VALIDATE_NAME;
                return;
            }
            // 校验手机号
            if (!/^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test($scope.list[2].value)) {
                $scope.errorTip = $scope.errorMsgList.NO_VALIDATE_PHONE;
                return;
            }
            // 校验执行利率
            if (!/^\d+(\.\d+)?$/.test($scope.list[4].value)) {
                $scope.errorTip = $scope.errorMsgList.NO_VALIDATE_RATE;
                return;
            }
            // 若校验通过，则清空错误提示信息
            $scope.errorTip = '';

            // 如果当前是新增操作
            if ($scope.type == 'add') {
                $scope.addNewInstitution();
                return;
            }
            $scope.updateInstitution();
        };

        /**
         * 提交新增的机构
         */
        $scope.addNewInstitution = function () {
            // 请求参数
            api.apiAddNewInstitution({
                institution_name: $scope.list[0].value,
                institution_linkman: $scope.list[1].value,
                institution_linkphone: $scope.list[2].value,
                institution_addr: $scope.list[3].value,
                institution_base_rate: $scope.list[4].value
            }).then(function (resp) {
                // 如果处理成功
                if (resp.data.code == $CONST.SUCCESS) {
                    $toast('新增机构信息成功', function () {
                        // 关闭弹出框
                        $scope.show = false;
                        // 刷新列表页面
                        $scope.getInstitutionList();
                    });
                }
            })
        };

        /**
         * 提交编辑后的机构
         */
        $scope.updateInstitution = function () {
            // 请求参数
            api.apiEditInstitution({
                id: $scope.insitutionItem.id,
                institution_name: $scope.list[0].value,
                institution_linkman: $scope.list[1].value,
                institution_linkphone: $scope.list[2].value,
                institution_addr: $scope.list[3].value,
                institution_base_rate: $scope.list[4].value
            }).then(function (resp) {
                // 如果处理成功
                if (resp.data.code == $CONST.SUCCESS) {
                    $toast('编辑机构信息成功', function () {
                        // 关闭弹出框
                        $scope.show = false;
                        // 刷新列表页面
                        $scope.getInstitutionList();
                    });
                }
            })
        };

        /**
         * 删除机构
         */
        $scope.deleteInsitution = function (id) {
            // 请求删除机构接口
            api.apiDeleteInsitution(id).success(function () {
                // 弹框提示，删除机构成功
                $toast('删除机构成功', function () {
                    // 刷新列表页面
                    $scope.getInstitutionList();
                })
            });
        };

        /**
         * 点击分页的时候，展示分页
         */
        $scope.changePage = function (page) {
            // 标记当前页
            $scope.currentPage = page;
            // 获取机构列表
            $scope.getInstitutionList();
        };

        /**
         * 获取机构列表
         */
        $scope.getInstitutionList = function (type) {
            if (type == 1) {
                $scope.currentPage = 1;
            }
            // 请求接口
            api.apiGetInstitutionList({
                currentPage: $scope.currentPage,
                institution_name: encodeURI($scope.institutionName || ''),
                pageSize: $scope.pageSize
            }).then(function (resp) {
                // 机构列表
                $scope.contentList = resp.data.data.institutionInfo;
                // 当前页
                $scope.currentPage = resp.data.data.currentPage;
                // 总页码
                $scope.totalPage = resp.data.data.totalPage;
            })
        };

        // 加载页面即请求接口
        $scope.getInstitutionList();
    });