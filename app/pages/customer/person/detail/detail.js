angular.module('myApp.pages.customer.person.detail', [])
// 定义路由
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/customer/person/detail/:id', {
        templateUrl: 'pages/customer/person/detail/detail.html',
        controller: 'CustomerPersonDetailCtrl'
    })
}])
// 定义控制器
.controller('CustomerPersonDetailCtrl', function($scope, $routeParams, api, $location) {
    // 当前页
    $scope.currentPage = 1;
    // 每页展示数量
    $scope.pageSize = 5;
    // 总页码
    $scope.totalPage = 1;
    // 不展示弹出框
    $scope.show = false;

    /**
     * 获取贷款人的贷款详细信息列表
     */
    $scope.getPersonCustomerDetailList = function() {
        api.getPersonalCustomerDetail({
            user_id: $routeParams.id,
            user_type: 1,
            currentPage: $scope.currentPage,
            pageSize: $scope.pageSize
        }).success(function(resp) {
            // 当前页
            $scope.currentPage = resp.data.currentPage;
            // 总页码
            $scope.totalPage = resp.data.totalPage;
            // 列表
            $scope.contentList = resp.data.customInfo;
        });
    };

    /**
     * 改变页码
     */
    $scope.changePage = function(page) {
        // 记录当前页
        $scope.currentPage = page;
        //
        $scope.getPersonCustomerDetailList();
    };

    /**
     * 跳转到贷款详情页面
     *
     * @param { Object } item 当前操作的对象
     */
    $scope.toLoanPersonDetail = function(item) {
        $location.path('/loan/person/detail/' + item.id + '/' + item.user_id)
    };

    // 页面一加载就请求接口
    $scope.getPersonCustomerDetailList();
});
