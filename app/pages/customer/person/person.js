angular.module('myApp.pages.customer.person', [])
// 定义路由
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/customer/person', {
        templateUrl: 'pages/customer/person/person.html',
        controller: 'CustomerPersonCtrl'
    })
}])
// 定义控制器
.controller('CustomerPersonCtrl', function($scope, $location, $toast, api) {
    // 待搜索的贷款人
    $scope.userName = '';
    // 当前页
    $scope.currentPage = 1;
    // 每页展示的数据
    $scope.pageSize = 10;
    // 总页码
    $scope.totalPage = 1;
    // 不展示弹出框
    $scope.show = false;

    /**
     * 获取客户管理中，个人管理的列表信息
     */
    $scope.getPersonCustomerList = function(type) {
        if (type == 1) {
            $scope.currentPage = 1;
        }
        // 获取信息
        api.getPersonalCustomerList({
            id: $scope.personId,
            user_name: $scope.userName,
            currentPage: $scope.currentPage,
            pageSize: $scope.pageSize
        }).success(function(resp) {
            // 记录当前页
            $scope.currentPage = resp.data.currentPage;
            // 记录总页数
            $scope.totalPage = resp.data.totalPage;
            // 将个人信息放到全局作用域中
            $scope.contentList = resp.data.customInfo;
        });
    };

    /**
     * 跳转到贷款详情页面
     *
     * @param { Object } item 当前贷款人信息
     */
    $scope.toLoanDetail = function(item) {
        // 如果用户id不存在
        if (!item.user_id) {
            $toast('用户信息不齐全，请联系管理员');
            return;
        }
        $location.path('/customer/person/detail/' + item.user_id);
    };

    /**
     * 改变页码
     *
     * @param { Number } page 当前点击的页码
     */
    $scope.changePage = function(page) {
        // 记录当前页
        $scope.currentPage = page;
        // 请求数据
        $scope.getPersonCustomerList();
    };

    /**
     * 跳转到好下单的购物订单列表页面
     *
     * @param { Object } item 当前操作的贷款人信息
     */
    $scope.toBuyOrderList = function(item) {
        $location.path('/order/list/' + item.user_phone + '/' + (item.user_id_number || 0));
    };

    // 获取个人管理的信息数据
    $scope.getPersonCustomerList();
});
