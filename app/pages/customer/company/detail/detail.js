angular.module('myApp.pages.customer.company.detail', [])
// 定义路由
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/customer/company/detail/:id', {
        templateUrl: 'pages/customer/company/detail/detail.html',
        controller: 'CustomerCompanyDetailCtrl'
    })
}])
// 定义控制器
.controller('CustomerCompanyDetailCtrl', function($scope, api, $routeParams, $CONST, $location) {
    // 当前页
    $scope.currentPage = 1;
    // 每页展示数量
    $scope.pageSize = 5;
    // 总页码
    $scope.totalPage = 1;
    // 不展示弹出框
    $scope.show = false;

    // 获取常量
    $scope.$CONST = $.extend(true, {}, $CONST);

    /**
     * 获取贷款人的贷款详细信息列表
     */
    $scope.getPersonCustomerDetailList = function() {
        api.getCompanyCustomerDeatil({
            user_id: $routeParams.id,
            user_type: 2,
            currentPage: $scope.currentPage,
            pageSize: $scope.pageSize
        }).success(function(resp) {
            // 当前页
            $scope.currentPage = resp.data.currentPage;
            // 总页码
            $scope.totalPage = resp.data.totalPage;
            // 列表
            $scope.contentList = resp.data.customInfo;
        });
    };

    /**
     * 改变页码
     */
    $scope.changePage = function(page) {
        // 记录当前页
        $scope.currentPage = page;
        //
        $scope.getPersonCustomerDetailList();
    };

    /**
     * 跳转到贷款详情页面
     *
     * @param { Object } item 当前操作的对象
     */
    $scope.toLoanCompanyDetail = function(item) {
        $location.path('/loan/company/detail/' + item.id + '/' + item.user_id)
    };

    // 页面一加载就请求接口
    $scope.getPersonCustomerDetailList();
});
