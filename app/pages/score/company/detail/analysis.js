angular.module('myApp.pages.score.analysis', [])
// 定义路由
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/score/company/analysis', {
        templateUrl: 'pages/score/company/detail/analysis.html',
        controller: 'ScoreCompanyAnalysisCtrl'
    })
}])
// 企业评分分析页面控制器
.controller('ScoreCompanyAnalysisCtrl', function($scope) {
    /**
     * 生成环形图
     */
    $scope.createDoughnutChart = function(data) {
        var ctx = document.getElementById("myChart").getContext("2d");
        new Chart(ctx).Doughnut(data, { segmentStrokeWidth: 1, percentageInnerCutout: 80 });
    };

    $scope.data = [
        { value: 10, color: '#fce6c8' },
        { value: 90, color: '#f5ac48' }
    ];

    $scope.createDoughnutChart($scope.data)
});