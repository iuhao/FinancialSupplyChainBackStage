angular.module('myApp.pages.score.company.target', [])
// 定义路由
      .config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('/score/company/target', {
                  templateUrl: 'pages/score/company/detail/target.html',
                  controller: 'ScoreCompanyTargetCtrl'
            })
      }])
      // 自定义控制器
      .controller('ScoreCompanyTargetCtrl', function ($scope,$CONST,$http,$routeParams,$location) {
            // 左侧菜单集合
            $scope.menuList = [];

            // 默认展示第一个左侧菜单
            $scope.currentIndex = 0;
            // 从session中获取financialIndex
            $scope.allFinancialIndex = session.get('allFinancialIndex');
            //财务指标下的得分
            $scope.financialIndex = $scope.allFinancialIndex.noFinancialIndexList[0];
            //非财务指标下的菜单
            $scope.unFinancialIndex = $scope.allFinancialIndex.noIndexListNew;
            //非财务指标下的左侧菜单
            $scope.menuList = $scope.unFinancialIndex;

            // 提交数据
            $scope.subList = []

            $scope.list = {};

            // 右侧内容
            $scope.titieList = [];

            angular.forEach($scope.menuList, function (menu) {
                  $scope.item = menu;
                  $scope.titieList.push($scope.item);
            });

            $scope.selectedMenuList = [
                  {
                        "personQuality": {
                              "quality": 1,
                              "creditRecord": 1,
                              "management": 1,
                              "operationTime": 1,
                              "education": 1,
                              "health": 1
                        }
                  },
                  {
                        "goodsAttribute": {
                              "channelLevel": 1,
                              "goodsType": 1,
                              "brandPopularity": 1,
                              "shelfLife": 1
                        }
                  },
                  {
                        "repayment": {
                              "cumulativeInventoryRatio":1,
                              "guarantee": 1,
                              "receivable": 1
                        }
                  },
                  {
                        "companyQuality": {
                              "registeredCapital": 1,
                              "managementTeam": 1,
                              "workerQuality": 1,
                              "marketCompete": 1,
                              "operateYears": 1,
                              "comCreditRecord": 1,
                              "customerCooperation": 1
                        }
                  },
                  {
                        "commonlyTrade": {
                              "sustainable": 1,
                              "salseGrowth": 1
                        }
                  },
                  {
                        "wholesaleTrade": {
                              "marketManage": 1,
                              "sustainable": 1,
                              "jdSaleChannel": 1
                        }
                  },
                  {
                        "makePolicy": {
                              "industryPolicy": 1,
                              "environment": 1,
                              "development": 1
                        }
                  },
                  {
                        "makeIndex": {
                              "sustainable": 1,
                              "deviceStart": 1
                        }
                  },
                  {
                        "retailTrade": {
                              "sustainable": 1,
                              "locationAdvantage": 1
                        }
                  }
            ];

            /**
             * 处理选择的菜单
             *
             * @param { Object } menu 当前点击的菜单
             * @param { Object } name 当前参数的名称
             */
            $scope.handlerCheckedMenu = function(menu, name) {
                  angular.forEach($scope.selectedMenuList, function(firstMenu) {
                        // 如果当前属性中包括次字段
                        if (firstMenu[$scope.menuList[$scope.currentIndex].e_name]) {
                              firstMenu[$scope.menuList[$scope.currentIndex].e_name][name] = parseInt(menu.score);
                        }
                  });
                  // console.log($scope.selectedMenuList);
            };

            $scope.handlerCurrentTarget = function ($index, eName) {
                  $scope.currentIndex = $index;
                  // 获取以及标题

            };
            //得分数组

            $scope.on = function (val) {
                  $scope.list.name=val.target.getAttribute('key');
                  $scope.list.key = val.target.value;
            }
            //提交分数
            $scope.submitScore = function () {
                  console.log($scope.selectedMenuList)
                  console.log($scope.selectedMenuList);
                  $http({
                        method:'POST',
                        url:$CONST.$IP+'score/v1/addCompanyOtherIndex',
                        data:{
                              companyId:$routeParams.id,
                              company_name: decodeURIComponent($routeParams.name),
                              otherIndex:$scope.selectedMenuList
                        }
                  }).then(function successCallback(response) {
                        $location.path('/score/company/analysis');
                  }, function errorCallback(response) {
                        // 请求失败执行代码
                  });
            }

      });