angular.module('myApp.pages.score.company.detail', [])
// 定义路由
      .config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('/score/company/detail/:id/:name', {
                  templateUrl: 'pages/score/company/detail/detail.html',
                  controller: 'ScoreCompanyDetailCtrl'
            })
      }])
      // 定义控制器
      .controller('ScoreCompanyDetailCtrl', function ($scope,$http,$location, $CONST, $timeout, $routeParams, $toast) {
            // 行业选择
            $scope.tradeInfo  = '1';
            // 不展示弹出框
            $scope.show = false;
            $scope.title = '提示';
            $scope.list = [{
                  type: 'toast', value: '确定上传'
            }];
            $scope.scorFileVal={};
            //财务指标和非财务指标数据
            $scope.allFinancialIndex = {};

            //
            /**
             * 获取行业列表
             * IndustrySelection  行业选择
             */
            $scope.getIndustryList = function () {
                  $http({
                        method: 'GET',
                        url: $CONST.$IP+'score/v1/getScoreIfName'
                  }).then(function success(res) {
                        $scope.scoreName = res.data.data.scoreName;
                  }, function error(response) {
                  });
            }
            $scope.getIndustryList();

            /**
             * 下载通用模板
             */

            $scope.downloadNormalTemplate = function () {
                  window.open('https://image.baidu.com/search/detail?ct=503316480&z=undefined&tn=baiduimagedetail&ipn=d&word=%E5%9B%BE%E7%89%87&step_word=&ie=utf-8&in=&cl=undefined&lm=undefined&st=undefined&cs=594559231,2167829292&os=2394225117,7942915&simid=3436308227,304878115&pn=1&rn=1&di=112970650540&ln=1980&fr=&fmq=1514187669282_R&fm=&ic=undefined&s=undefined&se=&sme=&tab=0&width=undefined&height=undefined&face=undefined&is=0,0&istype=0&ist=&jit=&bdtype=0&spn=0&pi=0&gsm=0&objurl=http%3A%2F%2Fimg.taopic.com%2Fuploads%2Fallimg%2F120727%2F201995-120HG1030762.jpg&rpstart=0&rpnum=0&adpicid=0')
            };

            /**
             * 提交上传的模板文件
             */
            $scope.handlerUploadTemplate = function ($event) {
                  // 如果没有选择文件，则不提交表单
                  if (!$event.value) {
                        return;
                  } else {
                        // debugger
                        $scope.scorFileVal=$event.value;
                        $timeout(function () {
                              $scope.show = true;
                        }, 50)
                  }
            };
            //上传已填好模板文件
            $scope.uploadScoreFile = function () {
                  // 提交表单（上传excel文档）
                  $('#template-file-upload').ajaxSubmit({
                        // console.log($CONST);
                        url: $CONST.$IP + 'score/v1/addCompanyFinanceIndex',
                        data:{
                              id:$scope.tradeInfo,
                              companyId: $routeParams.id,
                              company_name: decodeURIComponent($routeParams.name)
                        },
                        success: function (res) {
                              //将财务指标和非财务指标保存到session
                              $scope.allFinancialIndex=res.data;
                              $toast('上传成功');
                              session.set('allFinancialIndex', $scope.allFinancialIndex)
                              console.log($scope.allFinancialIndex);
                              // 跳转页面
                              $location.path('/score/company/target');
                              // $scope.$apply();
                        }
                        // error:function (res) {
                        //       $toast('上传失败');
                        //       $location.reload();
                        // }
                  });
                  $scope.show = false;
            }
            /**
             * 跳转到分析数据页面
             */
            $scope.toAnalysisDetail = function () {
                  $location.path('/score/company/analysis')
            };
      });