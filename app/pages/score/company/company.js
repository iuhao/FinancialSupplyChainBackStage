angular.module('myApp.pages.score.company', [])
// 定义路由
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/score/company', {
        templateUrl: 'pages/score/company/company.html',
        controller: 'ScoreCompanyCtrl'
    })
}])
// 定义一个控制器
.controller('ScoreCompanyCtrl', function($scope, $location, api, $user) {
      // 当前页
      $scope.currentPage = 1;
      // 每页展示的数据
      $scope.pageSize = 10;
      // 总页码
      $scope.totalPage = 1;
      // 评分状态选择，0为未选择
      $scope.rate_state='';
      $scope.companyName=''
      
      $scope.getCompanyRateList = function () {
            // console.log($scope.rate_state,$scope.companyName)
            // 数据重置，生成新的分页
            $scope.rateList = {};//评分列表
            $scope.totalPage = 1;
           api.apiCompanyRateList({
                 company_name: $scope.companyName,
                 gradeStatus: $scope.rate_state,
                  currentPage: $scope.currentPage,
                  pageSize: $scope.pageSize
            }).success(function(resp) {
                  // 获取数据
                  $scope.scoreList = resp.data.scoreList;
                  // 设置当前页
                  $scope.currentPage = resp.data.currentPage;
                  // 设置总页数
                  $scope.totalPage = resp.data.totalPage;
            });
      };
      $scope.getCompanyRateList();

    /**
     * 跳转到评分详情页面
     *
     * @param { Object } item 评分对象
     */

      /**
       * 改变页码
       */
      $scope.changePage = function(page) {
            // 记录当前页
            $scope.currentPage = page;
            //
            $scope.getCompanyRateList();
      };
      $scope.toScoreDetail = function(item) {
        $location.path('/score/company/detail/' + item.id + '/' + encodeURIComponent(item.company_name))
    }
});