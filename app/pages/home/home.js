angular.module('myApp.pages.home', [])

// 定义路由
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'pages/home/home.html',
        controller: 'HomeCtrl'
    })
}])
// 定义控制器
.controller('HomeCtrl', function($scope, $user, $location) {
    // 如果没有登录过，则直接跳转到首页
    if (!$user.get('accessToken')) {
        $location.path('/login');
    }
    // 标记已经登录了
    $scope.$emit('isLogin');
});