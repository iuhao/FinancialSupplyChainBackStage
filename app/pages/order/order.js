angular.module('myApp.pages.order', ['ngRoute'])
// 定义路由
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/order/list/:phone/:id', {
        templateUrl: 'pages/order/order.html',
        controller: 'OrderCtrl'
    });
}])
// 定义控制器
.controller('OrderCtrl', function($scope, $routeParams, api) {
    // 机构名称
    $scope.institutionName = {};

    // 遍历数据
    api.apiAllInstitutionNames().success(function(resp) {
        // 保存返回的机构名称
        $scope.institutionNames = resp.data.names;
        // 默认展示第一个
        $scope.institutionName = resp.data.names[0];
        // 获取订单列表
        $scope.getOrderListByInstitutionId();
    });

    /**
     * 通过机构id获取订单列表
     */
    $scope.getOrderListByInstitutionId = function() {
        api.apiOrderList($scope.institutionName.id, $routeParams.phone, $routeParams.id).success(function(resp) {
            $scope.orderList = resp.data.orderList.orderInfo;
            $scope.totalOrder = resp.data.orderList.totalOrder;
        });
    };
});