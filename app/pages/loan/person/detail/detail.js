// 个人贷款详情
angular.module('myApp.pages.loan.person.detail', [])
// 定义路由
    .config(['$routeProvider', function ($routeProvider) {
        // 带参数（详情）
        $routeProvider.when('/loan/person/detail/:id/:user_id', {
            templateUrl: 'pages/loan/person/detail/detail.html',
            controller: 'LoanPersonDetailCtrl'
        });
        // 带参数（审核）
        $routeProvider.when('/loan/person/detail/:id/:user_id/:is_audit', {
            templateUrl: 'pages/loan/person/detail/detail.html',
            controller: 'LoanPersonDetailCtrl'
        });
        $routeProvider.when('/loan/person/detail', {
            templateUrl: 'pages/loan/person/detail/detail.html',
            controller: 'LoanPersonDetailCtrl'
        })
    }])
// 定义控制器
    .controller('LoanPersonDetailCtrl', function ($scope, $location, $valid, $status, $CONST, $model, $toast, $timeout, $user, $http, api, $routeParams) {
        // 默认展示通过
        $scope.loanStatus = '2';
        // 贷款金额
        $scope.loanAmount = '';
        // 贷款失败描述
        $scope.loanDesc = '';
        // 贷款到期日期
        $scope.sdate = '';
        // 不展示弹出框
        $scope.show = false;

        // 获取当前时间并将其转换成字符串
        $scope.currentDateStr = new Date().format('yyyy-MM-dd');

        // 常量
        $scope.$CONST = $CONST;
        // 状态
        $scope.$status = $status;
        // 获取个人贷款详情内容
        $scope.getPersonalLoanDetail = function () {
            $scope.data = {
                id: $routeParams.id,
                user_type: 1,
                user_id: $routeParams.user_id,
                isAudit: $routeParams.is_audit
            };
            api.apiGetPersonalLoanDetail($scope.data).then(function (resp) {
                $scope.loanDetail = resp.data.data;
                $scope.reviewStatus();
            })
        };

        // 获取操作日志
        api.apiOperationLog($routeParams.id, 1).success(function (resp) {
            $scope.records = resp.data.records;
        });
        /*
         * 判断审核结果显示
         * 根据emloyee-status > clerk-status > load-status优先级判断
         * */
        $scope.reviewStatus = function () {
            return $scope.status = $scope.loanDetail.employee_status ? $scope.loanDetail.employee_status : ($scope.loanDetail.clerk_status ? $scope.loanDetail.clerk_status : $scope.loanDetail.loan_status)
            //if ($scope.loanDetail.employee_status) {
            //    return $scope.loanDetail.employee_status;
            //} else if ($scope.loanDetail.clerk_status) {
            //    return $scope.loanDetail.clerk_status;
            //} else {
            //    return $scope.loanDetail.loan_status;
            //}
        };

        /*
         * 判断审核原因显示
         * 根据emloyee-status > clerk-status > load-status优先级判断
         * */
        $scope.reviewCause = function () {
            if ($scope.loanDetail.employee_desc) {
                return $scope.loanDetail.employee_desc;
            } else if ($scope.loanDetail.clerk_desc) {
                return $scope.loanDetail.clerk_desc;
            } else {
                return $scope.loanDetail.loan_desc
            }
        };

        /**
         * 提交贷款结果
         */
        $scope.submitLoanResult = function () {
            // 如果选择了授信通过，但是没有填写授信金额
            if ($scope.loanStatus == 2 && !$scope.sdate) {
                $toast('请设置贷款到期日期');
                return;
            }
            // 如果选择了授信通过，但是没有填写授信金额
            if ($scope.loanStatus == 2 && !$scope.loanAmount) {
                $toast('请填写贷款建议额度');
                return;
            }
            // 贷款金额校验
            var errorMessage = $valid['money']($scope.loanAmount);
            // 如果校验失败
            if ($scope.loanStatus == 2 && errorMessage) {
                $toast(errorMessage);
                return;
            }
            // 如果选择了授信失败，但是没有填写原因
            if ($scope.loanStatus == 3 && !$scope.loanDesc) {
                $toast('请填写审核不通过原因');
                return;
            }
            // 如果选择授信不通过，则重置贷款到期时间为空
            if ($scope.loanStatus == 3) {
                $scope.sdate = '';
                $scope.loanAmount = '';
            }
            // 审核授信
            api.auditLoanCommit({
                id: $routeParams.id,
                loan_service_charge: $scope.loanDetail.loan_rate,
                loan_status: $scope.loanStatus,
                amount: $scope.loanAmount,
                loan_desc: $scope.loanDesc,
                user_id: $scope.loanDetail.user_id,
                sdate: $scope.sdate,
                user_role: $user.get('user_role'),
                // 1：个人 2：企业
                user_type: '1',
                type: 1,
                operator_id: $user.get('id')
            }).success(function (resp) {
                if (resp.code == 375) {
                    $location.path('/loan/person');
                    return
                }
                $toast('贷款审核成功', function () {
                    // 跳转到授信列表页
                    $location.path('/loan/person');
                });
            });
        };

        // 等待页面渲染完成，触发事件
        $timeout(function () {
            $(".img-view").viewer({
                url: 'data-original'
            });
        }, 100);

        // 页面加载调取接口
        $scope.getPersonalLoanDetail();
    });
