/*
 * 企业贷款列表也
 * */
var page = 1, pageSize = 10;

angular.module('myApp.pages.loan.company', [])
// 定义路由
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/loan/company', {
            templateUrl: 'pages/loan/company/company.html',
            controller: 'LoanCompanyCtrl'
        })
    }])
// 定义控制器
    .controller('LoanCompanyCtrl', function ($scope, $location, $institute, $model, $toast, $timeout, $user, $status, $http, api, $CONST) {
        // 页面内容
        $scope.contentList = [];
        // 当前页码
        $scope.currentPage = 1;
        // 当前页码
        $scope.totalPage = 0;
        // 每页展示的数目
        $scope.pageSize = 10;
        // 从session中获取账户的角色
        $scope.userRole = $user.get('user_role');
        // 常量
        $scope.$CONST = $.extend(true, {}, $CONST);
        // 贷款人姓名
        $scope.userName = '';
        // 贷款开始时间
        $scope.creditApplyTimeStart = '';
        // 贷款结束时间
        $scope.creditApplyTimeEnd = '';
        // 贷款状态
        $scope.loan_status = '';
        // 不展示弹出框
        $scope.show = false;
        // 列表
        $scope.list = [{type: 'toast', value: '是否确认通知客户？'}];

        // 关于状态的操作服务
        $scope.$status = $status;

        // 获取机构名称
        $scope.instituteNames = $institute.getInst();
        $scope.instituteName = '';

        /**
         * 切换状态
         *
         * @param { Object } item 当前的对象
         */
        $scope.switchState = function (item) {
            // 将所有的状态全都置为未选中
            angular.forEach($scope.$CONST.LOAN_STATE, function (state) {
                state.selected = false;
            });
            // 将点击的状态选中
            item.selected = true;
            // 保存贷款状态
            $scope.loan_status = item.value;
            // 搜索列表
            $scope.doSearch(1);
        };

        /**
         * 展示通知客户提示框
         *
         * @param { Object } item 保存当前项
         */
        $scope.popUpNotify = function (item) {
            // 将当前需要通知的项保存下来
            $scope.notifyItem = item;
            // 展示弹框
            $scope.show = true;
            // 弹框打开方式
            $scope.openType = 'tip';
            // 弹出框标题
            $scope.title = '贷款信息';
        };

        /**
         * 通知客户
         */
        $scope.notifyClients = function () {
            // 如果当前是利息，则点击确定，关闭弹框
            if ($scope.openType == 'interest') {
                $scope.show = false;
                return;
            }
            // 通知客户
            api.auditLoanCommit({
                id: $scope.notifyItem.id,
                operator_id: $user.get('id'),
                user_role: $user.get('user_role'),
                user_id: $scope.notifyItem.user_id,
                loan_status: $scope.notifyItem.employee_status,
                user_type: 2,
                amount: $scope.notifyItem.amount || $scope.notifyItem.employee_limit,
                type: 2
            }).success(function () {
                $model._toast('已成功通知客户');
                $scope.show = false;
            }).error(function() {
                // 其他错误，关闭弹框
                $scope.show = false;
            });
        };

        /**
         * 获取机构名称
         *
         * @param { Number } id 机构id
         */
        $scope.getInstituteName = function (id) {
            // 机构名称
            var name = '';
            // 循环出机构名称
            angular.forEach($scope.instituteNames, function (value) {
                if (value.value == id) {
                    name = value.label;
                }
            });
            return name;
        };


        /**
         * 打开查看利息弹出框
         *
         * @param { Object } item 当前操作的贷款对象
         */
        $scope.toInterestDetail = function (item) {
            // 查看利息
            api.apiTnterestQuery({
                id_type: item.id_type,
                idNo: item.id_no,
                page_size: 1,
                page_num: 1,
                debet_id: item.appno,
                end_date: item.end_data,
                order_id: item.order_id
            }).success(function (resp) {
                // 获取信息缺少
                if (!resp.data.head) {
                    $toast('未能成功从银行获取利息信息，请重试');
                    return;
                }
                // 返回报文
                var respBody = resp.data.head;
                // 展示弹出框
                $scope.show = true;
                // 弹出框标题
                $scope.title = '贷款信息';
                // 弹框打开方式
                $scope.openType = 'interest';
                // 列表
                $scope.list = [
                    {
                        type: 'text-group',
                        list: [
                            {label: '合同号', value: (respBody.MAST_CONTNO || '未填写')},
                            {label: '借据序号', value: (respBody.DEBET_NO || '未填写')},
                            {label: '借据编号', value: (respBody.DEBET_ID || '未填写')},
                            {label: '订单编号', value: (respBody.ORDER_ID || '未填写')},
                            {label: '借据状态', value: (respBody.LNCI_STATUS || '未填写'), color: '#f7ad3c'},
                            {label: '贷款金额', value: (respBody.LOAN_AMT || '未填写')},
                            {label: '还款日期', value: (respBody.REPAY_DATE || '未填写')},
                            {label: '正常利息', value: (respBody.NOR_INTEREST || '未填写')},
                            {label: '贷款日期', value: (respBody.LOAN_DATE || '未填写')},
                            {label: '证件类型', value: (respBody.ID_TYPE || '未填写')},
                            {label: '证件号码', value: (respBody.ID || '未填写')}
                        ]
                    }
                ];
            });
        };

        /**
         * 跳转到详情页面
         *
         * @param { Object } item    当前操作的对象
         * @param { Number } isAudit 当前是否是审核
         */
        $scope.toDetail = function (item, isAudit) {
            // 如果是审核按钮点进来的
            if (isAudit == 1) {
                $location.path('/loan/company/detail/' + item.id + '/' + item.user_id + '/' + isAudit);
                return;
            }
            // 标记当前是详情的
            $location.path('/loan/company/detail/' + item.id + '/' + item.user_id);
        };

        /*
         * 获取企业贷款列表数据
         */
        $scope.doSearch = function (type) {
            //debugger;
            if (type == 1) {
                $scope.currentPage = 1;
            }
            // 重置数据，生成新的分页
            $scope.loanList = {};
            $scope.totalPage = 1;
            var userId = session.get('login-user').id;
            var userRole = session.get('login-user').user_role;
            if (userRole == '17' || userRole == '1') {
                userRole = '';
                userId = '';
            }
            api.getPersonalList({
                // 名称
                company_name: $scope.userName,
                // 申请开始时间
                credit_apply_time_start: $scope.creditApplyTimeStart ? ($scope.creditApplyTimeStart + " 0:0:0") : "",
                // 申请结束时间
                credit_apply_time_end: $scope.creditApplyTimeEnd ? ($scope.creditApplyTimeEnd + " 23:59:59") : "",
                // 贷款状态
                type: $scope.loan_status,
                // 当前是后台接口
                version_type: '0',
                // 当前页
                currentPage: $scope.currentPage,
                // 每页展示数量
                pageSize: $scope.pageSize,
                user_type: 2,
                user_id: userId,
                user_role: userRole
            }).then(function (resp) {
                $scope.loanList = resp.data.data.loanList;
                $scope.currentPage = resp.data.data.currentPage;
                $scope.totalPage = resp.data.data.totalPage;
            });
        };

        /**
         * 改变分页
         *
         * @param { Number } page 分页组件穿过来的页码
         */
        $scope.changePage = function (page) {
            $scope.currentPage = page;
            $scope.doSearch();
        };

        // 默认请求接口
        $scope.doSearch();
    });
