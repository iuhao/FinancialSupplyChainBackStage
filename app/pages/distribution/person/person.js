angular.module('myApp.pages.distribution.person', [])
// 定义路由
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/distribution/person', {
        templateUrl: 'pages/distribution/person/person.html',
        controller: 'DistributionPersonCtrl'
    })
}])
// 定义控制器
.controller('DistributionPersonCtrl', function($scope, $CONST, $toast, api) {
    // 电话号码
    $scope.userPhone = '';
    // 待分配类型
    $scope.type = '';
    // 当前页
    $scope.currentPage = 1;
    // 每页展示数量
    $scope.pageSize = 10;
    // 总页码
    $scope.totalPage = 1;
    // 不展示弹出框
    $scope.show = false;

    // 柜员列表（查询接口完成后，会将值塞进去）
    $scope.list = [
        {
            type: 'radio-group',
            value: '',
            name: 'radio-group',
            list: []
        }
    ];

    // 深拷贝常量类
    $scope.$CONST = $.extend(true, {}, $CONST);

    /**
     * 改变状态
     *
     * @param { Number } $index 当前操作的对象序列
     */
    $scope.changeState = function($index) {
        // 遍历数据
        angular.forEach($scope.$CONST.ALLOT_STATE, function(value, index) {
            // 首先取消选中
            value.selected = false;
            // 如果当前是点击的那个，则选中
            if (index == $index) {
                $scope.type = value.value;
                value.selected = true;
            }
        });
        // 请求接口，加载数据
        $scope.getAllCustomersByType();
    };

    // 获取柜员信息
    api.apiAllClerk().success(function(resp) {
        // 遍历数据
        angular.forEach(resp.data.clerk, function(value) {
            $scope.list[0].list.push({
                label: ((value.CLERK_NO || '') + ' ' + (value.USER_NAME || value.USER_LOGIN_NAME)),
                value: (value.ID || '')
            });
        });
    });

    /**
     * 获取分配的用户列表
     */
    $scope.getAllCustomersByType = function() {
        api.apiAllCustomersByType({
            user_type: 'P',
            user_phone: $scope.userPhone,
            type: $scope.type,
            currentPage: $scope.currentPage,
            pageSize: $scope.pageSize
        }).success(function(data) {
            $scope.infos = data.data.customers;
            $scope.currentPage = data.data.currentPage;
            $scope.totalPage = data.data.total_page;
        });
    };

    /**
     * 将时间类型装换成字符串
     *
     * @param { Number } time 时间戳
     */
    $scope.dateToStr = function(time) {
        return new Date(time).format('yyyy-MM-dd hh:mm:ss');
    };

    /**
     * 分页组件改变了分页后触发事件
     *
     * @param { Number } page 当前页
     */
    $scope.changePage = function(page) {
        $scope.currentPage = page;
        $scope.getAllCustomersByType();
    };

    /**
     * 通过参数来搜索
     */
    $scope.searchByParamsAndReset = function() {
        $scope.currentPage = 1;
        // 将状态重置到全部
        $scope.changeState(0);
    };

    /**
     * 打开新增弹框页面
     *
     * @param { Object } item 当前操作的对象
     */
    $scope.addPopUp = function(item) {
        // 保存当前值
        $scope.selectedItem = item;
        // 展示弹框
        $scope.show = true;
        // 清空数据
        $scope.list[0].value = '';
    };

    /**
     * 打开编辑弹框页面
     *
     * @param { Object } item 当前操作的对象
     */
    $scope.editPopUp = function(item) {
        // 保存当前值
        $scope.selectedItem = item;
        // 展示弹框
        $scope.show = true;
        // 清空数据
        $scope.list[0].value = item.CLERK_PERSION;
    };

    /**
     * 绑定柜员信息
     */
    $scope.addOrUpdate = function() {
        // 如果柜员没有选择
        if (!$scope.list[0].value) {
            $toast('请选择柜员');
            return;
        }
        // 绑定柜员信息
        api.apiDistributionCustomer($scope.list[0].value, $scope.selectedItem.ID).success(function() {
            $toast('已经成功将客户分配到柜员', function() {
                // 关闭弹框
                $scope.show = false;
            });
            // 加载数据
            $scope.getAllCustomersByType();
        });
    };

    $scope.getAllCustomersByType();
});
