angular.module('myApp.pages.award.company', [])
// 定义路由
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/award/company', {
        templateUrl: 'pages/award/company/company.html',
        controller: 'AwardCompanyCtrl'
    })
}])
// 定义控制器
.controller('AwardCompanyCtrl', function($scope, $CONST, $user, $location, $toast, $status, api) {
    // 授信企业名称
    $scope.companyName = '';
    // 授信开始时间
    $scope.creditApplyTimeStart = '';
    // 授信结束时间
    $scope.creditApplyTimeEnd = '';
    // 当前页
    $scope.currentPage = 1;
    // 每页展示的数目
    $scope.pageSize = 10;
    // 总页数
    $scope.totalPage = 1;
    // 不展示弹出框
    $scope.show = false;

    // 深拷贝常量类
    $scope.$CONST = $.extend(true, {}, $CONST);

    // 状态
    $scope.$status = $status;

    /**
     * 切换状态
     *
     * @param { Object } item 当前的对象
     */
    $scope.switchState = function(item) {
        // 将所有的状态全都置为未选中
        angular.forEach($scope.$CONST.LOAN_STATE, function(state) {
            state.selected = false;
        });
        // 将点击的状态选中
        item.selected = true;
        // 保存贷款状态
        $scope.type = item.value;
        // 搜索列表
        $scope.getCompanyClientList(1);
    };

    /**
     * 跳转到授信详情页面中
     *
     * @param { Object } item    授信信息
     * @param { Number } isAudit 是否是审核状态
     */
    $scope.toDetail = function(item, isAudit) {
        // 如果当前页面是审核
        if (isAudit == 1) {
            $location.path('/award/company/detail/' + item.id + '/' + isAudit);
            return;
        }
        // 跳转到查看详情页面
        $location.path('/award/company/detail/' + item.id);
    };

    /**
     * 展示通知客户的弹出框
     *
     * @params { Object } item 当前点击的授信对象
     */
    $scope.popUpNotify = function(item) {
        // 弹出框展示数据
        $scope.list = [
            { label: '是否确定通知客户？', value: '是否确定通知客户', type: 'toast' }
        ];
        // 保存当前点击的item
        $scope.notifyItem = item;
        // 展示弹出框
        $scope.show = true;
    };

    /**
     * 通知客户
     */
    $scope.notifyClients = function() {
        // 通知客户
        api.apiCheck({
            id: $scope.notifyItem.id,
            operator_id: $user.get('id'),
            audit_status: $scope.notifyItem.employee_status,
            user_role: $user.get('user_role'),
            user_id:$scope.notifyItem.user_id,
            type: 2
        }).success(function() {
            $toast('已成功通知客户', function() {
                // 隐藏弹出框
                $scope.show = false;
            });
        }).error(function() {
            $scope.show = false;
        });
    };

    /**
     * 获取公司授信列表
     */
    $scope.getCompanyClientList = function(type) {
        if (type == 1) {
            $scope.currentPage = 1
        }
        // 数据重置，生成新的分页
        $scope.creditList = {};
        $scope.totalPage = 1;
        // 请求接口数据
        api.apiCompanyClientList({
            company_name: $scope.companyName,
            user_role: $user.get('user_role'),
            user_id: $user.get('id'),
            version_type: '0',
            access_token: $user.get('accessToken'),
            credit_apply_time_start: $scope.creditApplyTimeStart? $scope.creditApplyTimeStart + ' 00:00:00': '',
            credit_apply_time_end: $scope.creditApplyTimeEnd? $scope.creditApplyTimeEnd + ' 23:59:59': '',
            type: $scope.type,
            currentPage: $scope.currentPage,
            pageSize: $scope.pageSize
        }).success(function(resp) {
            // 获取数据
            $scope.creditList = resp.data.creditList;
            // 设置当前页
            $scope.currentPage = resp.data.currentPage;
            // 设置总页数
            $scope.totalPage = resp.data.totalPage;
        });
    };

    /**
     * 点击分页组件中的页码
     *
     * @param { Number } page 当前页
     */
    $scope.changePage = function(page) {
        $scope.currentPage = page;
        $scope.getCompanyClientList();
    };

    // 获取公司授信列表
    $scope.getCompanyClientList();
});
