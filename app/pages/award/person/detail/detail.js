angular.module('myApp.pages.award.person.detail', [])
// 定义路由
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/award/person/detail/:id', {
            templateUrl: 'pages/award/person/detail/detail.html',
            controller: 'AwardPersonDetailCtrl'
        });
        $routeProvider.when('/award/person/detail/:id/:isAudit', {
            templateUrl: 'pages/award/person/detail/detail.html',
            controller: 'AwardPersonDetailCtrl'
        });
    }])
// 定义控制器
    .controller('AwardPersonDetailCtrl', function ($scope, $routeParams, $valid, $user, $toast, $status, $CONST, $location, $timeout, api) {
        // 默认展示通过
        $scope.loanStatus = '2';
        // 授信建议额度
        $scope.loanAmount = '';
        // 授信失败原因
        $scope.loanDesc = '';
        // 如果当前是审核页面
        $scope.isAudit = $routeParams.isAudit;
        // 不展示弹出框
        $scope.show = false;
        // 获取常量
        $scope.$CONST = $CONST;

        // 状态服务
        $scope.$status = $status;

        /**
         * 获取个人授信详细信息
         */
        $scope.getPersonAwardDetail = function () {
            // 获取
            api.apiCreditPersonDetail($routeParams.id).success(function (data) {
                $scope.info = data.data;
            });
        };

        // 获取操作日志
        api.apiOperationLog($routeParams.id, 'P').success(function (resp) {
            $scope.records = resp.data.records;
        });

        /**
         * 提交授信结果
         */
        $scope.submitAwardResult = function () {
            // 如果选择了授信通过，但是没有填写授信金额
            if ($scope.loanStatus == 2 && !$scope.loanAmount) {
                $toast('请填写授信建议额度');
                return;
            }
            // 贷款金额校验
            var errorMessage = $valid['money']($scope.loanAmount);
            // 如果校验失败
            if ($scope.loanStatus == 2 && errorMessage) {
                $toast(errorMessage);
                return;
            }
            // 如果选择了授信失败，但是没有填写原因
            if ($scope.loanStatus == 3 && !$scope.loanDesc) {
                $toast('请填写审核不通过原因');
                return;
            }
            if ($scope.loanStatus == 3) {
                $scope.loanAmount = ''
            }
            // 审核授信
            api.apiCreditPersonExamine({
                id: $routeParams.id,
                audit_status: $scope.loanStatus,
                credit_limit: $scope.loanAmount,
                audit_desc: $scope.loanDesc,
                user_id: $scope.info.user_id,
                user_role: $user.get('user_role'),
                type: 1,
                operator_id: $user.get('id')
            }).success(function (resp) {
                if (resp.code == 376) {
                    $location.path('/award/person');
                    return
                }
                $toast('授信审核成功', function () {
                    // 跳转到授信列表页
                    $location.path('/award/person');
                });
            });
        };

        /**
         * 跳转到订单列表页面
         */
        $scope.toOrderDetail = function () {
            $location.path('/order/list/' + $scope.info.user_phone + '/' + $scope.info.user_id_number);
        };

        /**
         * 下载文件
         *
         * @param { String } link 文件地址
         */
        $scope.downloadFile = function (link) {
            window.open(link);
        };

        // 等待页面渲染完成，触发事件
        $timeout(function () {
            $(".img-view").viewer({
                url: 'data-original'
            });
        });

        $scope.getPersonAwardDetail();
    });
