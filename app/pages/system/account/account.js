angular.module('myApp.pages.system.account', [])
// 定义路由
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/system/account', {
            templateUrl: 'pages/system/account/account.html',
            controller: 'SystemAccountCtrl'
        })
    }])
    // 定义控制器
    .controller('SystemAccountCtrl', function ($scope, api, $role, $CONST, $toast, $valid) {
        // 是否展示弹框
        $scope.show = false;

        // 获取角色列表
        $scope.roleList = $role.getRoles();

        // 请求参数
        $scope.requestParams = {
            id: '',
            user_login_name: '',
            currentPage: 1,
            pageSize: 10
        };

        // 设置回调
        $role.setCallback(function () {
            // 获取角色列表
            $scope.roleList = $role.getRoles();
        });

        // 管理员列表
        $scope.adminList = [];

        // 展示弹出框需要的数据
        $scope.popUpList = [];

        /**
         * 获取管理员列表
         */
        $scope.getAdminList = function () {
            // 请求数据
            api.apiAdminList($scope.requestParams).success(function (data) {
                $scope.adminList = data.data.adminInfo;
                $scope.totalPage = data.data.totalPage;
            });
        };

        /**
         * 编辑账户信息
         *
         * @param { Object } item 当前操作的对象
         */
        $scope.editAccountInfo = function (item) {
            // 当前账号id
            $scope.id = item.id;
            // 请求数据
            api.apiAdminList(Object.assign({}, $scope.requestParams, {id: item.id})).success(function (data) {
                // 获取账户信息
                $scope.editAccount = data.data.roleInfo[0];

                // 弹出框标题
                $scope.popUpTitle = '修改账户';

                // 标记当前是编辑
                $scope.type = 'edit';

                // 弹出框需要的数据
                $scope.popUpList = [
                    {
                        type: 'readonly',
                        label: '账号',
                        value: $scope.editAccount.login_name
                    },
                    {
                        type: 'input',
                        label: '手机号码',
                        value: $scope.editAccount.user_phone
                    }
                    //{
                    //    type: 'radio',
                    //    label: '选择角色',
                    //    name: 'chooseRole',
                    //    value: $scope.editAccount.role_list,
                    //    list: $scope.roleList
                    //}
                ];

                // 展示弹出框
                $scope.show = true;
            });
        };

        /**
         * 删除账号信息
         *
         * @param { Number } id 账号id
         */
        $scope.deleteAccountInfo = function (id) {
            // 删除一条账号信息
            api.deleteAccountInfo(id).success(function () {
                // 提示信息
                $toast('账号删除成功');
                // 获取账户信息
                $scope.getAdminList();
            });
        };

        /**
         * 新增账户信息
         */
        $scope.addAccountInfo = function () {
            // 弹出框标题
            $scope.popUpTitle = '新增账户';

            // 展示弹出框
            $scope.show = true;
            $scope.tip = '';
            // 标记当前是编辑
            $scope.type = 'add';

            // 弹出框需要的数据
            $scope.popUpList = [
                {
                    type: 'input',
                    label: '姓名',
                    value: '',
                    must: true
                },
                {
                    type: 'input',
                    label: '手机号码',
                    value: '',
                    must: true
                },
                {
                    type: 'radio',
                    label: '选择角色',
                    name: 'chooseRole',
                    value: 1,
                    list: $scope.roleList,
                    must: true
                }
            ];
        };

        /**
         * 关联柜员
         *
         * @param { Object } item 当前需要关联的账户
         */
        $scope.linkAccount = function (item) {
            // 获取所有的柜员信息
            api.getClerksInfo(item.id).success(function (data) {
                // 展示弹出框
                $scope.show = true;

                // 关联柜员
                $scope.popUpTitle = '关联柜员';

                // 关联柜员
                $scope.type = 'link';

                // 待绑定的信息
                $scope.bindClerkId = item.id;

                // 弹出框数据
                $scope.popUpList = [
                    {
                        type: 'text',
                        must: true,
                        list: ['账号：' + item.clerk_no, '角色：风控员']
                    },
                    {
                        type: 'checkbox',
                        label: '请勾选需要关联的柜员',
                        name: 'unbindclerk',
                        value: '',
                        must: true,
                        list: $scope.returnBoxData(data.data.unbindclerk)
                    },
                    {
                        type: 'checkbox',
                        label: '已关联柜员',
                        name: 'clerks',
                        value: '',
                        must: true,
                        list: $scope.returnBoxData(data.data.clerks)
                    }
                ];
            });
        };

        /*
         * 错误信息
         * */
        $scope.errorMsgList = {
            NO_NULL: '*为必填项',
            NO_VALIDATE_NAME: '格式错误，姓名为中文或英文',
            NO_VALIDATE_PHONE: '格式错误，手机号为11位数字'
        };

        /**
         * 弹出框点击提交时，调用此方法，提交修改的数据
         */
        $scope.submitAccountInfo = function () {
            // 校验姓名，手机号是否为空
            if ((!$scope.popUpList[0].value || !$scope.popUpList[1].value) && $scope.type != 'link') {
                $scope.errorTip = $scope.errorMsgList.NO_NULL;
                return;
            }
            // 当type为link时，校验是否选择了需要关联的柜员
            if (!$scope.popUpList[1].value && $scope.type == 'link') {
                $scope.errorTip = '请选择需要关联的柜员';
                return;
            }

            // 校验姓名是否符合正则
            if (!/^[\u4e00-\u9fa5a-zA-Z]+$/.test($scope.popUpList[0].value) && $scope.type == 'add') {
                $scope.errorTip = $scope.errorMsgList.NO_VALIDATE_NAME;
                return;
            }
            // 校验手机号
            if (!/^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test($scope.popUpList[1].value) && $scope.type != 'link') {
                $scope.errorTip = $scope.errorMsgList.NO_VALIDATE_PHONE;
                return;
            }
            // 若校验通过，则清空错误提示信息
            $scope.errorTip = '';
            // 如果当前是编辑状态
            if ($scope.type == 'edit') {
                api.editAccountInfo({
                    id: $scope.id,
                    //login_name: $scope.popUpList[1].value,
                    user_name: $scope.popUpList[0].value,
                    //role_list: $scope.popUpList[2].value,
                    user_phone: $scope.popUpList[1].value
                }).success(function () {
                    // 展示提示信息
                    $toast('编辑账户信息成功', function () {
                        // 关闭弹框
                        $scope.show = false;
                    });
                    // 刷新列表数据
                    $scope.getAdminList();
                });
            }

            // 如果当前是新增状态
            if ($scope.type == 'add') {
                api.addAccountInfo({
                    login_name: $scope.popUpList[1].value,
                    user_name: $scope.popUpList[0].value,
                    login_pass: $CONST.CONFIG.PWD,
                    role_list: $scope.popUpList[2].value,
                    user_phone: $scope.popUpList[1].value
                }).success(function () {
                    // 展示提示信息
                    $toast('新增账户信息成功', function () {
                        // 关闭弹框
                        $scope.show = false;
                    });
                    // 刷新列表数据
                    $scope.getAdminList();
                });
            }

            // 如果当前是关联柜员
            if ($scope.type == 'link') {
                api.apiBindRiskManagement({
                    riskManagement: $scope.bindClerkId,
                    clerks: $scope.popUpList[1].value.concat($scope.popUpList[2].value || [])
                }).success(function (resp) {
                    // 展示提示信息
                    $toast('关联柜员信息成功', function () {
                        // 关闭弹框
                        $scope.show = false;
                    });
                });
            }
        };

        /**
         * 返回符合弹出框条件的数据
         *
         * @param { Object } info 待装换的数据
         */
        $scope.returnBoxData = function (info) {
            // 返回的数据
            var returnArr = [];

            // 遍历数据
            angular.forEach(info, function (value) {
                returnArr.push({
                    label: (value.CLERK_NO || '') + ' ' + value.USER_NAME,
                    value: value.ID
                })
            });
            return returnArr;
        };

        /**
         * 点击分页内容
         *
         * @params { Number } page 页码
         */
        $scope.changePage = function (page) {
            $scope.requestParams.currentPage = page;
            $scope.getAdminList();
        };

        /*
        * 点击查询按钮，执行搜索操作，同时将当前页重置为1
        * */
        $scope.onSearch = function () {
            $scope.requestParams.currentPage = 1;
            $scope.getAdminList();
        };

        // 默认加载页面
        $scope.getAdminList();
    });
