angular.module('myApp.pages.system.role', [])
// 定义路由
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/system/role', {
        templateUrl: 'pages/system/role/role.html',
        controller: 'SystemRoleCtrl'
    })
}])
// 定义控制器
.controller('SystemRoleCtrl', function($scope, $role, api, $menu, $toast, $CONST) {
    // 不展示弹出框
    $scope.show = false;
    // 获取角色列表
    $scope.roleList = $role.getReturnRoles();

    // 没有获取到角色列表
    if ($scope.roleList.length == 0) {
        // 当角色列表获取到数据后，加载数据
        $role.setCallback(function() {
            // 获取角色列表
            $scope.roleList = $role.getReturnRoles();
        });
    }

    // 获取菜单列表
    $scope.menuList = $menu.getBoxMenu();

    // 没有获取到角色列表
    if ($scope.menuList.length == 0) {
        // 当角色列表获取到数据后，加载数据
        $menu.setCallback(function() {
            // 获取角色列表
            $scope.menuList = $menu.getBoxMenu();
        });
    }

    /**
     * 新增角色
     */
    $scope.addRole = function() {
        // 展示弹出框
        $scope.show = true;

        // 弹出框名称
        $scope.title = '新建角色';

        // 新增角色
        $scope.type = 'add';

        // 弹出框数据
        $scope.popUpList = [
            {
                type: 'input',
                label: '角色名称',
                value: ''
            },
            {
                type: 'checkbox-group',
                label: '权限设置',
                name: 'roleSet',
                value: [],
                list: $.extend(true, [], $scope.menuList)
            },
            {
                type: 'textarea',
                label: '角色描述',
                value: ''
            }
        ];
    };

    /**
     * 修改角色
     *
     * @param { Object } item 当前编辑的角色
     */
    $scope.modifyRole = function(item) {
        // 展示弹出框
        $scope.show = true;

        // 弹出框名称
        $scope.title = '修改角色';

        // 新增角色
        $scope.type = 'edit';

        // 保存当前id
        $scope.id = item.id;

        // 弹出框数据
        $scope.popUpList = [
            {
                type: 'input',
                label: '角色名称',
                value: item.role_name
            },
            {
                type: 'checkbox-group',
                label: '权限设置',
                name: 'roleSet',
                value: item.auth_list.split(','),
                list: $.extend(true, [], $scope.menuList)
            },
            {
                type: 'textarea',
                label: '角色描述',
                value: item.role_desc
            }
        ];
    };

    /**
     * 删除角色
     *
     * @param { Object } item 当前编辑的角色
     */
    $scope.deleteRole = function(item) {
        // 删除角色信息
        api.deleteRoleInfo(item.id).success(function() {
            // 提示信息
            $toast('删除角色成功');
            // 刷新角色列表
            $role.refress();
        });
    };

    /**
     * 展示角色
     *
     * @param { Number } roleId 角色id
     */
    $scope.showRole = function(roleId) {
        return !$CONST.WARNING_ROLE.inArray(roleId);
    };

    /**
     * 修改角色
     */
    $scope.submitRoleInfo = function() {
        // 如果是编辑状态
        if ($scope.type == 'edit') {
            api.editRoleInfo({
                id: $scope.id,
                role_name: $scope.popUpList[0].value,
                role_desc: $scope.popUpList[2].value,
                auth_list: $scope.popUpList[1].value.join(',')
            }).success(function() {
                // 提示编辑角色成功
                $toast('编辑角色成功', function() {
                    $scope.show = false;
                });
                // 刷新角色列表
                $role.refress();
            });
        }

        // 如果是新增状态
        if ($scope.type == 'add') {
            api.addRoleInfo({
                role_name: $scope.popUpList[0].value,
                role_desc: $scope.popUpList[2].value,
                auth_list: $scope.popUpList[1].value.join(',')
            }).success(function() {
                // 提示编辑角色成功
                $toast('新增角色成功', function() {
                    $scope.show = false;
                });
                // 刷新角色列表
                $role.refress();
            });
        }
    };
});
