'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.config',
  'myApp.filter.all',
  'myApp.directive.breadCrumb',
  'myApp.directive.header',
  'myApp.directive.input',
  'myApp.directive.leftNav',
  'myApp.directive.list',
  'myApp.directive.login',
  'myApp.directive.page',
  'myApp.directive.other',
  'myApp.directive.popUp',
  'myApp.directive.text',
  'myApp.directive.appTextGroup',
  'myApp.directive.radio',
  'myApp.directive.radioGroup',
  'myApp.directive.checkbox',
  'myApp.directive.checkboxGroup',
  'myApp.services.api',
  'myApp.services.userInfo',
  'myApp.services.toast',
  'myApp.services.validate',
  'myApp.services.role',
  'myApp.services.menu',
  'myApp.services.status',
  'myApp.services.institute',
  'myApp.pages.home',
  'myApp.pages.award.company',
  'myApp.pages.award.company.detail',
  'myApp.pages.award.person',
  'myApp.pages.award.person.detail',
  'myApp.pages.customer.company',
  'myApp.pages.customer.company.detail',
  'myApp.pages.customer.person',
  'myApp.pages.customer.person.detail',
  'myApp.pages.distribution.person',
  'myApp.pages.distribution.company',
  'myApp.pages.institution',
  'myApp.pages.loan.company',
  'myApp.pages.loan.company.detail',
  'myApp.pages.loan.person',
  'myApp.pages.loan.person.detail',
  'myApp.pages.login',
  'myApp.pages.system.account',
  'myApp.pages.system.role',
  'myApp.pages.order',
  'myApp.pages.score.company',
  'myApp.pages.score.company.detail',
  'myApp.pages.score.company.target',
  'myApp.pages.score.analysis',
  'ngDraggable',
  'myApp.const.all'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/home'});
}]).
run(['$rootScope', function($rootScope) {
  // 监听菜单变化
  $rootScope.$on('menu-change', function(event, data) {
    $rootScope.menus = data;
  });
}]);
