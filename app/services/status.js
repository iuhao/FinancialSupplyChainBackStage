angular.module('myApp.services.status', [])
// 定义状态服务
      .service('$status', function ($user) {

            /**
             * 获取状态名称
             *
             * @params { Number } clerkStatus    柜员审核状态
             * @params { Number } employeeStatus 风控审核状态
             */
            this.name = function (clerkStatus, employeeStatus) {
                  if (employeeStatus == 2) {
                        return '审核通过'
                  }
                  if (employeeStatus == 3) {
                        return '审核不通过'
                  }
                  if (employeeStatus == 4) {
                        return '已放款'
                  }
                  if (employeeStatus == 5) {
                        return '已还款'
                  }
                  if (!clerkStatus || clerkStatus == 1 || clerkStatus == 0) {
                        return '待审核'
                  }
                  if (clerkStatus == 2 && (!employeeStatus || employeeStatus == 1)) {
                        return '审核通过'
                  }
                  if (clerkStatus == 3) {
                        return '审核不通过'
                  }
            };

            /**
             * 是否展示审核按妞
             *
             * @params { Object } item 授信信息
             */
            this.showAuditButton = function (item) {
                  // 如果当前是柜员，且未被审核过，则展示审核按钮
                  if ($user.isCounter() && (!item.clerk_status || item.clerk_status == 1 || item.clerk_status == 0)) {
                        return true;
                  }
                  // 如果当前是风控，且未被审核过，则展示审核按钮
                  if ($user.isRiskManagement() && (item.clerk_status == 2 || item.clerk_status == 3) && (!item.employee_status || item.employee_status == 1 || item.employee_status == 0)) {
                        return true;
                  }
                  return false;
            };

            /**
             * 是否展示查看详情按妞
             *
             * @params { Object } item 授信信息
             */
            this.showDetailButton = function (item) {
                  // 如果当前是柜员，如果审核过，则展示查看详情
                  if ($user.isCounter() && (item.clerk_status == 2 || item.clerk_status == 3)) {
                        return true;
                  }
                  // 如果当前是风控，如果审核过，则展示查看详情
                  if ($user.isRiskManagement()) {
                        // 如果当前是风控审核成功或失败，则展示查看详情按钮
                        if (item.employee_status == 2 || item.employee_status == 3) {
                              return true;
                        }
                        // 如果当前是柜员待审核，则展示查看详情按钮
                        if (!item.clerk_status || item.clerk_status == 0 || item.clerk_status == 1) {
                              return true;
                        }
                        return false;
                  }
                  return false;
            };

            /**
             * 审核成功
             *
             * @params { Object } item 授信信息
             */
            this.auditSuccess = function (item) {
                  // 如果当前值不存在
                  if (!item) {
                        return false;
                  }
                  // 如果当前是风控，且未被审核过，则展示审核按钮
                  if (item.employee_status == 2) {
                        return true;
                  }
                  // 如果当前是柜员，且未被审核过，则展示审核按钮
                  if (item.clerk_status == 2 && (item.employee_status != 3 && item.employee_status != 2)) {
                        return true;
                  }
                  return false;
            };

            /**
             * 审核失败
             *
             * @params { Object } item 授信信息
             */
            this.auditFail = function (item) {
                  // 如果当前值不存在
                  if (!item) {
                        return false;
                  }
                  // 如果当前是风控，且未被审核过，则展示审核按钮
                  if (item.employee_status == 3) {
                        return true;
                  }
                  // 如果当前是柜员，且未被审核过，则展示审核按钮
                  if (item.clerk_status == 3 && (item.employee_status != 3 && item.employee_status != 2)) {
                        return true;
                  }
                  return false;
            };

            /**
             * 展示通知客户
             *
             * @params { Object } item 授信信息
             */
            this.showToastCustomer = function (item) {
                  // 如果当前是柜员，且未被审核过，则展示审核按钮
                  if ($user.isCounter() && (item.employee_status == 2 || item.employee_status == 3)) {
                        return true;
                  }
                  return false;
            };

            /**
             * 展示查看利息按钮
             *
             * @params { Object } item 授信贷款信息
             */
            this.showInterest = function (item) {
                  // 如果风控审核通过且是订单贷款
                  if (item.employee_status == 2 && item.lnci_type == 1) {
                        return true;
                  }
                  return false;
            };

            /**
             * 通过角色id和状态编码来确定状态字段
             *
             * @params { Number } roleCode 状态编码
             * @params { Number } status   状态编码
             */
            this.getStatus = function (roleCode, status) {
                  // 如果当前是柜员
                  if ($user.isCounter(roleCode)) {
                        return this.name(status);
                  }
                  // 如果当前是
                  if ($user.isRiskManagement(roleCode)) {
                        return this.name(2, status);
                  }
            };
            /*
             * 根据状态码返回‘柜员’，‘风控’
             * @params { Number } roleCode 状态编码
             * */
            this.getRoleName = function (roleCode) {
                  // 如果当前是柜员
                  if ($user.isCounter(roleCode)) {
                        return '柜员';
                  }
                  // 如果当前是
                  if ($user.isRiskManagement(roleCode)) {
                        return "风控";
                  }
            };

            /**
             * 通过角色id和状态编码来确定状态字段
             *
             * @params { Number } status   状态编码
             */
            this.getSelfStatus = function (status) {
                  // 如果当前是柜员
                  if ($user.isCounter()) {
                        return this.name(status);
                  }
                  // 如果当前是
                  if ($user.isRiskManagement()) {
                        return this.name(2, status);
                  }
            };
      });