angular.module('myApp.services.role', [])
// 角色对象
.service('$role', function(api) {
    // 角色列表
    var roleList = [];

    // 后台获取的角色列表
    var returnRoleList = [];

    // 回调函数
    var callBack = [];

    // 获取角色列表
    var getRoleList = function() {
        // 获取角色列表
        api.getRoleList().success(function(data) {
            // 先将数据清空
            roleList = [];

            // 保存返回的信息
            returnRoleList = data.data.roleInfo;

            // 如果没有返回角色信息，则不处理
            if (!data.data.roleInfo || data.data.roleInfo.length == 0) {
                return;
            }
            // 遍历数据
            angular.forEach(data.data.roleInfo, function(value) {
                roleList.push({
                    value: value.id,
                    label: value.role_name
                })
            });
            // 回调
            angular.forEach(callBack, function(value) {
                value();
            });
        });
    };


    /**
     * 设置回调方法
     */
    this.setCallback = function(fn) {
        callBack.push(fn);
    };

    /**
     * 刷新数据
     */
    this.refress = function() {
        getRoleList();
    };

    /**
     * 返回处理后的角色列表
     */
    this.getRoles = function() {
        return roleList;
    };

    /**
     * 直接返回角色的列表
     */
    this.getReturnRoles = function() {
        return returnRoleList;
    };

    getRoleList();
});