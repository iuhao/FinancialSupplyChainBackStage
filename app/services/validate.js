angular.module('myApp.services.validate', [])
// 校验js
.service('$valid', function() {
    return {
        name: function(str) {
            if (!str) {
                return '请输入姓名';
            }
            // 校验通过
            if (/^[\u4e00-\u9fa5]+(·[\u4e00-\u9fa5]+)+[0-9]*$ /.test(str)) {
                return '';
            }
            return '格式错误，姓名不包含特殊字符';
        },
        // 电话校验
        phone: function(number) {
            // 校验成功
            if (/^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test(number)) {
                return '';
            }
            return '格式错误，请输入11位的手机号码'
        },
        // 密码验证
        pwd: function(str) {
            // 密码没有输入
            if (!str) {
                return '请输入密码';
            }
            // 验证成功
            if (/^(?!\d+$|[a-zA-Z]+$|[\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\x7f]+$)/.test(str)) {
               return '';
            }
            return '格式错误，密码为6-12位数字、字母2者的组合'
        },
        // 验证码
        VC: function(str) {
            // 验证成功
            if (/^[0-9A-Za-z]{4}$/.test(str)) {
                return;
            }
            return '格式错误，请输入4位由数字或字母组成的验证码';
        },
        // 短信验证码
        PC: function(str) {
            // 校验成功
            if (/^\d{6}$/.test(str)) {
                return;
            }
            return '格式错误，请输入6位数字';
        },
        // 身份证号校验
        ID: function(number) {
            // 校验成功
            if (/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(number)) {
                return '';
            }
            return '格式错误，请输入18位证件号码，最后一位为数字或X';
        },
        // 固定电话校验
        tel: function(number) {
            if (/^([0-9]{3,4}-)?[0-9]{7,8}$/.test(number)) {
                return '';
            }
            return '格式错误，请输入3-4位区号以及号码'
        },
        // 金额
        money: function(number) {
            // 校验成功
            if (/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/.test(number)) {
                return '';
            }
            // 校验失败提示
            return '格式错误，请输入合法的金额'
        },
        // 邮箱
        email: function(str) {
            // 校验成功
            if (/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/.test(str)) {
                return ''
            }
            return '格式错误，请输入邮箱，必须有@和.'
        },
        // 社会信用号
        SCN: function(str) {
            // 校验成功
            if (/[0-9A-HJ-NPQRTUWXY]{2}\d{6}[0-9A-HJ-NPQRTUWXY]{10}/.test(str)) {
                return '';
            }
            return '格式错误，请输入18位社会信用号，由数字或大写字母组成';
        },
        // 组织机构代码
        OC: function(str) {
            //if (/^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{1}$/.test(str)) {
            //    return '';
            //}
            //return '格式错误，请输入由8位数字或拉丁字母和“-”和一位数字或拉丁字母'
            return '';
        },
        // 营业执照号
        BLN: function(str) {
            // 校验成功
            if (/^\w{15}$/.test(str)) {
                return '';
            }
            // 校验成功
            if (/^\w{18}$/.test(str)) {
                return '';
            }
            return '格式错误，请输入15位营业执照号，只能由数字组成'
        },
        // 纳税人识别号
        TRN: function(str) {
            // 校验成功
            if (/^\d{6}[0-9A-Z]{9}$/.test(str)) {
                return ''
            }
            return '格式错误，请输入15位纳税人识别号，由6位行政区划码（纯数字）+ 9位组织机构代码（数字或大写字母）'
        }
    }
});