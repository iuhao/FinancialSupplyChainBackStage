angular.module('myApp.services.toast', [])

// 定义用户信息的服务
.service('$toast', function($timeout) {
    return function(content, callback) {
        var $html = $('<div class="toast-mask"><div class="toast">' + content + '</div></div>');
        $html.appendTo($("body")).fadeIn();
        $timeout(function () {
            $html.fadeOut();
            callback && callback();
        }, 1000);
    }
})
.service('$load', function() {
    return function(isShow, callback) {
        // 如果是加载动画
        if (isShow) {
            var $html = $('<div class="toast-mask"><div class="loading"><div class="loader-inner line-scale"><div></div><div></div><div></div><div></div><div></div></div></div></div>');
            $html.appendTo($("body")).fadeIn();
            callback && callback();
            return;
        }
        // 如果是移除动画，则删除动画
        $('.loading').parent().remove();
        callback && callback();
    }
})
/**
 * 自定义弹框组件
 *
 * 通过$model即可调用
 */
.service("$model", function ($timeout) {
    return {
        // toast
        _toast: function (content, callback) {
            var $html = $('<div class="toast-mask"><div class="toast">'+ content +'</div></div>');
            $html.appendTo($("body")).fadeIn();
            $timeout(function() {
                $html.fadeOut();
                callback && callback()
            }, 1000);
        }
    };
});