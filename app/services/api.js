angular.module('myApp.services.api', [])
// 请求对象
      .service('api', function ($http, $CONST) {
            return {
                  // 登录
                  apiLogin: function (params) {
                        return $http.post($CONST.$IP + 'system/v1/login', params);
                  },
                  // 修改密码
                  modifyPwd: function (params) {
                        return $http.post($CONST.$IP + 'system/v1/changePassword', params);
                  },
                  // 个人贷款列表
                  getPersonalList: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'loanManage/v1/loanList', params));
                  },
                  // 个人贷款详情
                  apiGetPersonalLoanDetail: function (params) {
                        return $http.get($CONST.$IP + 'loanManage/v1/LoanInfoPlat/' + params.id + '/' + params.user_type + '/' + params.user_id);
                  },
                  // 企业授信列表
                  apiCompanyClientList: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'creditManage/v1/creditCompanyList', params));
                  },

                  // 企业授信详情
                  apicreditCompanyDetail: function (id) {
                        return $http.get($CONST.$IP + 'creditManage/v1/creditCompany?id=' + id);
                  },
                  // 授信审核接口
                  apiCheck: function (params) {
                        return $http.post($CONST.$IP + 'creditManage/v1/creditCompanyExamine', params);
                  },
                  // 个人授信列表接口
                  getPersonClientList: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'creditManage/v1/creditPersonList', params));
                  },
                  // 个人授信详情
                  apiCreditPersonDetail: function (id) {
                        return $http.get($CONST.$IP + 'creditManage/v1/creditPerson?id=' + id);
                  },
                  // 机构管理
                  apiGetInstitutionList: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'institution/v1/institution', params));
                  },
                  // 新增机构
                  apiAddNewInstitution: function (params) {
                        return $http.post($CONST.$IP + 'institution/v1/institution', params);
                  },
                  // 编辑机构
                  apiEditInstitution: function (params) {
                        return $http.put($CONST.$IP + 'institution/v1/institution', params);
                  },
                  // 删除机构
                  apiDeleteInsitution: function (id) {
                        return $http.delete($CONST.$IP + 'institution/v1/institution/' + id);
                  },
                  // 客户管理个人
                  getPersonalCustomerList: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'custom/v1/customPersonList', params));
                  },
                  // 客户管理详情个人
                  getPersonalCustomerDetail: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'custom/v1/personLoan', params));
                  },
                  // 企业客户管理
                  getCompanyCustomerList: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'custom/v1/customCompanyList', params));
                  },
                  // 企业客户管理详情
                  getCompanyCustomerDeatil: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'custom/v1/companyLoan', params));
                  },
                  // 获取管理员列表
                  apiAdminList: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'system/v1/admin', params));
                  },
                  // 删除指定的账号
                  deleteAccountInfo: function (id) {
                        return $http.post($CONST.$IP + 'system/v1/admin/' + id, {});
                  },
                  // 新增一条账号信息
                  addAccountInfo: function (params) {
                        return $http.post($CONST.$IP + 'system/v1/admin', params);
                  },
                  // 新增一条账号信息
                  editAccountInfo: function (params) {
                        return $http.put($CONST.$IP + 'system/v1/admin', params);
                  },
                  // 获取角色列表
                  getRoleList: function () {
                        return $http.get($CONST.$IP + 'system/v1/role');
                  },
                  // 角色删除
                  deleteRoleInfo: function (id) {
                        return $http.delete($CONST.$IP + 'system/v1/role/' + id);
                  },
                  // 获取菜单列表
                  getMenuList: function () {
                        return $http.get($CONST.$IP + 'system/v1/menu');
                  },
                  // 编辑角色信息
                  editRoleInfo: function (params) {
                        return $http.put($CONST.$IP + 'system/v1/role', params);
                  },
                  // 新增角色信息
                  addRoleInfo: function (params) {
                        return $http.post($CONST.$IP + 'system/v1/role', params);
                  },
                  // 获取柜员绑定信息
                  getClerksInfo: function (employee_no) {
                        return $http.post($CONST.$IP + 'system/v1/getClerks', {employee_no: employee_no});
                  },
                  // 审核提交的贷款申请
                  auditLoanCommit: function (params) {
                        return $http.put($CONST.$IP + 'loanManage/v1/loanExamine', params);
                  },
                  // 获取客户管理中个人信息的列表
                  apiPersonCustomerList: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'custom/v1/customList', params));
                  },
                  // 获取贷款人的客户贷款列表
                  apiCustomerInfo: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'custom/v1/customInfo', params));
                  },
                  // 操作日志
                  apiOperationLog: function (id, type) {
                        return $http.post($CONST.$IP + 'system/v1/getFinancechainRecordByAuthIdAndType', {
                              id: id,
                              type: type
                        });
                  },
                  // 分配用户查询
                  apiAllCustomersByType: function (params) {
                        return $http.post($CONST.$IP + 'system/v1/getAllCustomersByType', params);
                  },
                  // 获取所有的柜员
                  apiAllClerk: function () {
                        return $http.post($CONST.$IP + 'system/v1/getAllClerks', {});
                  },
                  // 分配柜员到用户
                  apiDistributionCustomer: function (clerk_id, customer_id) {
                        return $http.post($CONST.$IP + 'system/v1/distributionCustomer', {
                              clerk_id: clerk_id,
                              customer_id: customer_id
                        });
                  },
                  // 个人授信审核
                  apiCreditPersonExamine: function (params) {
                        return $http.post($CONST.$IP + 'creditManage/v1/creditPersonExamine', params);
                  },
                  // 公司贷款
                  apiLoanCompanyExamine: function (params) {
                        return $http.put($CONST.$IP + 'loanManage/v1/loanCompanyExamine', params);
                  },
                  // 查看利息
                  apiTnterestQuery: function (params) {
                        return $http.post($CONST.$IP + 'third/v1/interestQuery', params);
                  },
                  // 获取机构名称
                  apiAllInstitutionNames: function () {
                        return $http.get($CONST.$IP + 'institution/v1/getAllInstitutionNames');
                  },
                  // 获取订单列表
                  apiOrderList: function (coreFirm, userPhone, idNumber) {
                        return $http.get($CONST.$IP + 'third/v1/orderList/' + userPhone + '/' + coreFirm);
                  },
                  // 校验图片验证码是否符合条件
                  apiCheckImageCode: function (codeToken, code) {
                        return $http.get($CONST.$IP + 'system/v1/checkCode/' + codeToken + '/' + code);
                  },
                  // 企业评分列表
                  apiCompanyRateList: function (params) {
                        return $http.get(appendToUrl($CONST.$IP + 'score/v1/getSorceCompanyList', params));
                  },
                  // 绑定柜员
                  apiBindRiskManagement: function (params) {
                        return $http.post($CONST.$IP + 'system/v1/bindRiskManagement', params);
                  }
            }
      });