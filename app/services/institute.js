angular.module('myApp.services.institute', [])
// 定义机构
.service('$institute', function(api) {
    // 机构名称
    var institutes = [];
    var callBack = [];

    // 请求接口
    api.apiAllInstitutionNames().success(function(data) {
        // 如果没有获取值
        if (!data.data.names || data.data.names.length == 0) {
            return;
        }
        // 遍历获取的值
        angular.forEach(data.data.names, function(value, index) {
            // 将值处理后放到机构中
            institutes.push({
                label: value.institution_name,
                value: value.id,
                selected: (index == 0)
            });
        });
        // 回调
        angular.forEach(callBack, function(value) {
            value();
        });
    });

    /**
     * 设置回调方法
     */
    this.setCallBack = function(fn) {
        callBack.push(fn);
    };

    /**
     * 获取机构信息
     */
    this.getInst = function() {
        return institutes;
    };
});