angular.module('myApp.services.userInfo', ['ngRoute'])

// 定义用户信息的服务
.service('$user', function() {
    // 重命名全局变量
    var $this = this;

    // 回调方法
    var callback = [];

    // 用户的登录信息
    this.userInfo = (session.get('login-user') || {});

    // 设置回调方法
    this.setCallback = function(fn) {
        callback.push(fn);
    };

    /**
     * 获取用户信息
     */
    this.setUser = function(userInfo) {
        // 如果用户信息不存在，或者用户信息是非对象类型的，则抛出一个错误
        if (!userInfo || typeof userInfo !== 'object') {
            throw new Error('请上传合法的用户信息');
            return;
        }
        // 设置用户信息
        $this.userInfo = userInfo;

        // 将用户信息保存到session中
        session.set('login-user', userInfo);

        // 执行回调方法
        angular.forEach(callback, function(fn) {
            fn && fn();
        });
    };

    /**
     * 返回登录信息
     */
    this.getUser = function() {
        return $this.userInfo;
    };

    /**
     * 获取用户登录信息中的某个属性
     */
    this.get = function(key) {
        return $this.userInfo[key];
    };

    /**
     * 判断当前用户是否是柜员
     *
     * @params { Number } userRole 用户角色
     */
    this.isCounter = function(userRole) {
        // 如果传入了userRole，则判断userRole
        if (userRole) {
            return userRole == 32;
        }
        return $this.userInfo.user_role == 32;
    };

    /**
     * 判断当前用户是否是风控
     *
     * @params { Number } userRole 用户角色
     */
    this.isRiskManagement = function(userRole) {
        // 如果传入了userRole，则判断userRole
        if (userRole) {
            return userRole == 33;
        }
        return $this.userInfo.user_role == 33;
    };

    /**
     * 移除用户的登录信息
     */
    this.removeUser = function() {
        session.remove('login-user');
    };
});