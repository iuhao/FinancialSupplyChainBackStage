angular.module('myApp.services.menu', [])
// 定义菜单项
.service('$menu', function(api, $user) {
    // 菜单列表
    var menuList = [];
    // 弹框展示的信息
    var menuBoxList = [];
    // 回调函数
    var callBack = [];

    // 菜单
    var menuGroup = [];

    // 获取菜单列表
    api.getMenuList().success(function(data) {
        // 如果菜单不存在，则直接抛出
        if (!data.data.list || data.data.list.length == 0) {
            return;
        }

        // 从登录信息中获取数据
        angular.forEach(data.data.list, function(value) {
            // 菜单
            var menu = {};
            var box = {};
            // 遍历数据
            angular.forEach(value, function(value1, index) {
                // 如果当前是父节点
                if (value1.auth_parent == 0) {
                    menu = {
                        id: value1.id,
                        name: value1.auth_name,
                        open: false,
                        children: []
                    };
                    box = {
                        label: value1.auth_name,
                        name: (value1.id + index),
                        list: []
                    };
                } else {
                    menu.children.push({
                        id: value1.id,
                        name: value1.auth_name,
                        href: value1.auth_url
                    });
                    box.list.push({
                        label: value1.auth_name,
                        value: value1.id
                    });
                }
            });
            // 将遍历出来的数据保存到列表中
            menuList.push(menu);
            menuBoxList.push(box);
        });

        // 回调
        angular.forEach(callBack, function(value) {
            value();
        });
    });

    /**
     * 设置回调方法
     */
    this.setCallback = function(fn) {
        callBack.push(fn);
    };

    /**
     * 获取用户登录信息中的用户所有的菜单id
     */
    this.getUserMenu = function() {
        // 先清空菜单
        menuGroup = [];

        // 从登录信息中获取数据
        angular.forEach($user.get('lists'), function(value) {
            // 菜单
            var menu = {};
            // 遍历数据
            angular.forEach(value, function(value1) {
                // 如果当前是父节点
                if (value1.auth_parent == 0) {
                    menu = {
                        id: value1.id,
                        name: value1.auth_name,
                        open: false,
                        children: []
                    };
                } else {
                    menu.children.push({
                        id: value1.id,
                        name: value1.auth_name,
                        href: value1.auth_url
                    });
                }
            });

            // 将遍历出来的数据保存到列表中
            menuGroup.push(menu);
        });
        return menuGroup;
    };

    // 获取菜单项
    this.getMenu = function() {
        return menuList;
    };

    // 获取菜单项
    this.getBoxMenu = function() {
        return menuBoxList;
    };
});