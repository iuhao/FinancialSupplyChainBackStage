-- 文件目录及功能 --
    assets                  静态文件存放目录（图片）

    bower_components        angular框架存放位置（系统生成）

    common                  本系统使用的公共文件
        My97DatePicker      时间插件
        config.js           配置文件（系统请求拦截器等）
        const.js            系统使用的常量（大部分）
        directive.js        自定义的指令
        filter.js           过滤器
        jquery.page.js      jquery插件
        jquery-1.11.0.min.js
        util.js             系统使用的公共类

    components              本系统使用的公共组件
        appCheckbox         复选框组件（原生复选框，主要是排列方式，用于popup组件中）
        appCheckboxGroup    复选框组件（原生复选框，主要是排列方式，用于popup组件中）
        appRadio            单选组件（原生单选按钮，主要是排列方式，用于popup组件中）
        appRadioGroup       单选框组件（原生单选框，主要是排列方式，用于popup组件中）
        appTextGroup        文本组合组件（主要是排列方式，用于popup组件中）
        breadCrumb          面包屑导航组件
        header              头部组件
        input               输入框组件（主要用户popup组件中）
        leftNav             左侧菜单组件
        list                表格组件（已废除）
        login               登录组件（和前台复用）
        page                分页组件
        popUp               弹出框组件
        text                文本组件（排列方式）
        version             系统创建

    pages                   系统的页面
        award               授信页面
            company         企业授信页面
                detail      企业授信详情页面
            person          个人授信页面
                detail      个人授信详情页面
        customer            客户管理页面
        distribution        分配管理页面
        home                首页管理页面
        institution         机构管理页面
        loan                贷款管理页面
        login               登录管理页面
        order               订单详情页面（在授信详情页面的我的订单点击进入）
        system              系统管理

    services                自定义服务（保存公共数据，方便各个组件使用）
        api.js              ajax请求数据服务
        institute.js        机构数据服务
        menu.js             菜单服务
        role.js             角色服务
        toast.js            吐司弹框服务（提示框等）
        userInfo.js         用户信息服务
        validate.js         输入框校验服务

    styles                  系统使用的其他css样式（每个页面的样式在每个页面对应的文件夹下）

    app.css                 公共css样式（在这个css样式中引入了组件的css样式）
    app.js                  系统入口文件（important）
    index.html              系统入口文件


